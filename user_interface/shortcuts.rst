.. meta::
   :description: Set your own shortcuts in Kdenlive video editor
   :keywords: KDE, Kdenlive, shortcuts, set, documentation, user manual, video editor, open source, free, learn, easy

.. metadata-placeholder

   :authors: - Annew (https://userbase.kde.org/User:Annew)
             - Claus Christensen
             - Yuri Chornoivan
             - Simon Eugster <simon.eu@gmail.com>
             - Ttguy (https://userbase.kde.org/User:Ttguy)
             - Thanks4theFish (https://userbase.kde.org/User:Thanks4theFish)
             - Bushuev (https://userbase.kde.org/User:Bushuev)

   :license: Creative Commons License SA 4.0

.. _shortcuts:

Keyboard Shortcuts
==================

.. contents::



..
  TODO:
  * Important shortcuts (i.e. excluding Ctrl-S for saving and other general shortcuts working everywhere)   


Editing
-------



.. list-table::
  :header-rows: 1

  * -  Action 
    -  Shortcut
  * -  Make a cut (make sure track is selected first) 
    -  :kbd:`Shift + R` 
  * -  Play 
    -  :kbd:`Space` 
  * -  Play zone 
    -  :kbd:`Ctrl + Space` 
  * -  Render 
    -  :kbd:`Ctrl + Return` 
  * -  Switch Monitor 
    -  :kbd:`T` 
  * -  Forward 
    -  :kbd:`L` 
  * -  Rewind 
    -  :kbd:`J` 
  * -  Forward 1 frame 
    -  :kbd:`Right` 
  * -  Rewind 1 frame 
    -  :kbd:`Left` 
  * -  Forward 1 second 
    -  :kbd:`Shift + Right` 
  * -  Rewind 1 second 
    -  :kbd:`Shift + Left` 
  * -  Toggle Full Screen Mode on and off 
    -  :kbd:`Ctrl + Shift + F` 
  * -  Go to Clip End 
    -  :kbd:`End` 
  * -  Go to Clip Start 
    -  :kbd:`Home` 
  * -  Go to Next Snap Point 
    -  :kbd:`Alt + Right` 
  * -  Go to Previous Snap Point 
    -  :kbd:`Alt + Left` 
  * -  Go to Project End 
    -  :kbd:`Ctrl + End` 
  * -  Go to Project Start 
    -  :kbd:`Ctrl + Home` 
  * -  Go to Zone End 
    -  :kbd:`Shift + O` 
  * -  Go to Zone Start 
    -  :kbd:`Shift + I` 
  * -  Group Clips 
    -  :kbd:`Ctrl + G` 
  * -  Set Zone In 
    -  :kbd:`I` 
  * -  Ungroup Clips 
    -  :kbd:`Ctrl + Shift + G` 
  * -  Set Zone Out 
    -  :kbd:`O` 
  * -  :ref:`editing` 
    - :kbd:`Ctrl + X` [1]_   


.. [1]  available in bleeding edge version > 0.9.10 (Jan2015)
