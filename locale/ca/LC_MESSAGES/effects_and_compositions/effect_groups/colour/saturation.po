# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___colour___saturation.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-06 14:10+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:14
msgid "Saturation"
msgstr "Saturació"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:16
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:18
msgid ""
"This is the `Frei0r saturat0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-saturat0r/>`_ MLT filter."
msgstr ""
"Aquest és el `filtre «saturat0r» de Frei0r <https://www.mltframework.org/"
"plugins/FilterFrei0r-saturat0r/>`_ del MLT."

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:20
msgid "Adjusts the saturation of a source image."
msgstr "Ajusta la saturació d'una imatge font."

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:22
msgid ""
"See `TheDiveo's blog <https://thediveo-e.blogspot.com/2013/10/grading-of-"
"hero-3-above-waterline.html>`_ for an example of the usage of the Saturation "
"effect."
msgstr ""
"Vegeu el `blog de TheDiveo <https://thediveo-e.blogspot.com/2013/10/grading-"
"of-hero-3-above-waterline.html>`_ per a un exemple d'ús de l'efecte de "
"saturació."

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:24
msgid "https://youtu.be/rWqlQaWtCFs"
msgstr "https://youtu.be/rWqlQaWtCFs"

#: ../../effects_and_compositions/effect_groups/colour/saturation.rst:26
msgid "https://youtu.be/reOG42ZzrZA"
msgstr "https://youtu.be/reOG42ZzrZA"
