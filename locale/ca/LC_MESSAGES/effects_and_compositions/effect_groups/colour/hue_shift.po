# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___colour___hue_shift.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 14:32+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:15
msgid "Hue Shift"
msgstr "Canvi de to"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:18
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:20
msgid ""
"This is the `Frei0r hueshift0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-hueshift0r/>`_ MLT filter."
msgstr ""
"Aquest és el `filtre «hueshift0r» de Frei0r <https://www.mltframework.org/"
"plugins/FilterFrei0r-hueshift0r/>`_ del MLT."

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:22
msgid "Shifts the hue of a source image."
msgstr "Canvia el to d'una imatge font."

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:24
msgid "https://youtu.be/Mq_G-AFznoc"
msgstr "https://youtu.be/Mq_G-AFznoc"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:26
msgid "https://youtu.be/J7RCdP0-4Qs"
msgstr "https://youtu.be/J7RCdP0-4Qs"

#: ../../effects_and_compositions/effect_groups/colour/hue_shift.rst:28
msgid "https://youtu.be/D9w-I8hb3kU"
msgstr "https://youtu.be/D9w-I8hb3kU"
