# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___misc___dance.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-12 13:45+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:10
msgid "Dance"
msgstr "Balla"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:12
msgid "Contents"
msgstr "Contingut"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:14
msgid "This effect causes the video frame to dance around the screen."
msgstr "Aquest efecte fa que el fotograma del vídeo balli per la pantalla."

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:16
msgid ""
"Add this effect to a video and include another video track below it and a "
"composite transition between the two tracks."
msgstr ""
"Afegiu aquest efecte a un vídeo i inclogueu una pista addicional de vídeo a "
"sota i una transició composta entre les dues pistes."

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:18
msgid "https://youtu.be/gqxU1nvh6JI"
msgstr "https://youtu.be/gqxU1nvh6JI"
