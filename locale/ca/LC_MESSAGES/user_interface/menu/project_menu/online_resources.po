# Translation of docs_kdenlive_org_user_interface___menu___project_menu___online_resources.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-21 09:30+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/project_menu/online_resources.rst:18
msgid "Online Resources"
msgstr "Recursos en línia"

#: ../../user_interface/menu/project_menu/online_resources.rst:20
msgid "Contents"
msgstr "Contingut"

#: ../../user_interface/menu/project_menu/online_resources.rst:22
msgid ""
"Available from the :ref:`project_menu` menu and the :ref:`clips` dropdown,  "
"this feature allows you to search online audio, video and graphics libraries "
"for resources to download."
msgstr ""
"Disponible des del menú :ref:`project_menu` i de la llista desplegable :ref:"
"`clips`. Aquesta característica permet cercar àudios en línia, vídeos i "
"biblioteques gràfiques de recursos per a baixar."

# skip-rule: t-acc_obe
#: ../../user_interface/menu/project_menu/online_resources.rst:32
msgid ""
"The new online resources window features more source footage providers such "
"as `Pixabay <https://pixabay.com/>`_ and `Pexels <https://www.pexels.com/>`_ "
"besides the already available `Freesound <https://freesound.org/>`_ and "
"`Internet Archive <https://archive.org/>`_. Other possible providers are "
"being considered, see `here <https://invent.kde.org/multimedia/kdenlive/-/"
"issues/918>`_ for more details."
msgstr ""
"La finestra nova de recursos en línia té més proveïdors de fonts de "
"metratges com `Pixabay <https://pixabay.com/>`_ i `Pexels <https://www."
"pexels.com/>`_ a banda dels ja disponibles `Freesound <https://freesound.org/"
">`_ i `Internet Archive <https://archive.org/>`_. S'estan considerant altres "
"possibles proveïdors, vegeu `aquí <https://invent.kde.org/multimedia/"
"kdenlive/-/issues/918>`_ per a més detalls."

#: ../../user_interface/menu/project_menu/online_resources.rst:38
msgid ""
"It is important to give credit to the downloaded sources so we’ve added an "
"option to directly import the license attribution as a project note."
msgstr ""
"És important donar crèdit a les fonts baixades, així que s'ha afegit una "
"opció per a importar directament l'atribució de la llicència com a nota del "
"projecte."

#: ../../user_interface/menu/project_menu/online_resources.rst:50
msgid "Freesound Audio Library"
msgstr "Freesound Audio Library (Biblioteca d'àudio Freesound)"

#: ../../user_interface/menu/project_menu/online_resources.rst:52
msgid ""
"The Freesound Audio Library part of this became non-functional on old "
"versions of Kdenlive in October 2015 because freesound now requires user "
"registration. If you are using an old version of Kdenlive, you can still "
"download great audio clips from `https://www.freesound.org/ <https://www."
"freesound.org/>`_ – just not through the Kdenlive interface."
msgstr ""
"Part de la Freesound Audio Library ha esdevingut no funcional en versions "
"antigues del Kdenlive l'octubre de 2015 perquè ara Freesound requereix el "
"registre de l'usuari. Si esteu usant una versió antiga del Kdenlive, encara "
"podeu baixar magnífics clips d'àudio des de `https://www.freesound.org/ "
"<https://www.freesound.org/>`_ – però no a través de la interfície del "
"Kdenlive."

#: ../../user_interface/menu/project_menu/online_resources.rst:58
msgid ""
"Version 15.12 (December 2015) allowed users to automatically download the "
"high-quality .mp3 preview file from freesound using the **Online Resources** "
"module."
msgstr ""
"La versió 15.12 (desembre 2015) permet que els usuaris baixin automàticament "
"el fitxer de vista prèvia «.mp3» d'alta qualitat des de Freesound usant el "
"mòdul **Recursos en línia**."

#: ../../user_interface/menu/project_menu/online_resources.rst:64
msgid "Freesound Audio Library - Future version"
msgstr "Freesound Audio Library - Versió futura"

#: ../../user_interface/menu/project_menu/online_resources.rst:66
msgid ""
"A future version of Kdenlive will allow you to authenticate Kdenlive with "
"your free freesound web account and download the high-quality files."
msgstr ""
"Una versió futura del Kdenlive permetrà autenticar el Kdenlive amb el vostre "
"compte web de Freesound i baixar els fitxers d'alta qualitat."

#: ../../user_interface/menu/project_menu/online_resources.rst:68
msgid ""
"If the **Online Resources** dialog offers up this error, Error Getting "
"Access Token from Freesound. Try importing again to obtain a new freesound "
"connection hit the import button again to make it try to establish a new "
"authentication handshake with the freesound website."
msgstr ""
"Si el diàleg **Recursos en línia** mostra aquest error (Error Getting Access "
"Token) de Freesound, intenteu importar una altra vegada per a aconseguir una "
"connexió amb Freesound. Premeu un altre cop el botó d'importació fer a "
"intentar establir una conformitat nova d'autenticació amb el lloc web de "
"Freesound."

#: ../../user_interface/menu/project_menu/online_resources.rst:73
msgid ""
"On hitting the import button, and if this is the first time you have used "
"the freesound library, you will be presented with a login dialog where you "
"can use your freesound account credentials to authenticate and download the "
"highest-quality version of the audio file. If you do not have a freesound "
"account, you can choose to download the high-quality .mp3 preview version of "
"the file instead."
msgstr ""
"En prémer el botó d'importació, i si aquesta és la primera vegada que s'usa "
"la biblioteca Freesound, se us presentarà un diàleg d'inici de sessió a on "
"podreu usar les vostres credencials del compte de Freesound per a autenticar-"
"se i baixar la versió de més alta qualitat del fitxer d'àudio. En canvi, si "
"no teniu cap compte de Freesound, podreu baixar la versió de vista prèvia «."
"mp3» en alta qualitat del fitxer."

#: ../../user_interface/menu/project_menu/online_resources.rst:79
msgid ""
"It might take a few seconds for the freesound web page to load in the window "
"– be patient."
msgstr ""
"Pot trigar alguns segons a carregar la pàgina web de Freesound en la "
"finestra – tingueu paciència."

#: ../../user_interface/menu/project_menu/online_resources.rst:81
msgid ""
"Once you have logged into the freesound system, you must grant Kdenlive "
"permission to access your freesound account."
msgstr ""
"Una vegada s'hagi iniciat la sessió en el sistema de Freesound, heu "
"d'atorgar permís d'accés al vostre compte de Freesound al Kdenlive."

#: ../../user_interface/menu/project_menu/online_resources.rst:89
msgid ""
"You can revoke Kdenlive's access to your freesound account from the "
"freesound website. Log in to your account and click the link in the App "
"permissions section on the settings page \"Manage your list of permissions "
"granted to API applications\" http://freesound.org/home/app_permissions/."
msgstr ""
"Podeu revocar l'accés de Kdenlive al vostre compte de Freesound des del seu "
"lloc web. Inicieu sessió amb el vostre compte i feu clic a l'enllaç de la "
"secció de permisos de l'aplicació a la pàgina de configuració «Manage your "
"list of permissions granted to API applications» http://freesound.org/home/"
"app_permissions/."

#: ../../user_interface/menu/project_menu/online_resources.rst:92
msgid ""
"Once permission is granted, Kdenlive obtains authentication keys that it "
"saves to the kdenlive config file (:file:`/~/.config/kdenliverc`). It uses "
"these keys to automatically authenticate on future file import requests. "
"Delete the tokens from the kdenlive config file to prevent this."
msgstr ""
"Una vegada atorgat el permís, el Kdenlive obté les claus d'autenticació que "
"es desen en el seu fitxer de configuració (:file:`/~/.config/kdenliverc`). "
"El Kdenlive usa aquestes claus per a autenticar-se automàticament en "
"peticions futures d'importació de fitxers. Suprimiu els testimonis del "
"fitxer de configuració del Kdenlive per a evitar-ho."

#: ../../user_interface/menu/project_menu/online_resources.rst:96
msgid "Archive Org Video Library"
msgstr "Archive Org Video Library (Biblioteca de vídeo d'Archive org)"

#: ../../user_interface/menu/project_menu/online_resources.rst:98
msgid ""
"This searches the internet archives video library for video files that you "
"can import into Kdenlive. Animated .gifs are displayed as video previews"
msgstr ""
"Això cerca fitxers de vídeo a la biblioteca de vídeo d'arxius d'Internet que "
"es poden importar en el Kdenlive. Els «.gifs» animats es mostren com a "
"vistes prèvies de vídeo"

#: ../../user_interface/menu/project_menu/online_resources.rst:103
msgid "Open Clip Art Graphic Library"
msgstr "Open Clip Art Graphic Library (Biblioteca gràfica d'art Open Clip)"

#: ../../user_interface/menu/project_menu/online_resources.rst:105
msgid ""
"This searches the Open Clip Art library for clip art that can be imported "
"into Kdenlive"
msgstr ""
"Això cerca clips gràfics a la biblioteca Open Clip Art que es poden importar "
"en el Kdenlive"
