# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-28 10:52+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:14
msgid "Analysis and Data - Vectorscope"
msgstr "Analyse en gegevens - Vectorscope"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:18
msgid ""
"This is the `Frei0r vectorscope <https://www.mltframework.org/plugins/"
"FilterFrei0r-vectorscope/>`_ MLT filter."
msgstr ""
"Dit is het MLT-filter `Frei0r vectorscope <https://www.mltframework.org/"
"plugins/FilterFrei0r-vectorscope/>`_."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:20
msgid "Displays the vectorscope of the video-data."
msgstr "Toont de vectorscope van de video-gegevens."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:22
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""
"In versie 17.04 is dit gevonden in de categorie :ref:`analysis_and_data` van "
"effecten."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:24
msgid ""
"It is recommended to use the vectorscope from :ref:`vectorscope`, because "
"the effect *Analysis and Data - Vectorscope* is not correct - it uses a "
"graticule from an analog NTSC vectorscope, but equations from digital video."
msgstr ""
"Het wordt aanbevolen de vectorscope uit :ref:`vectorscope` te gebruiken, "
"omdat het effect *Analyse en gegevens - Vectorscope* niet juist is - het "
"gebruikt een graticule van een analoge NTSC vectorscope, maar formules uit "
"digitale video."

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:26
msgid "https://youtu.be/2ybBzDEjdRo"
msgstr "https://youtu.be/2ybBzDEjdRo"

#: ../../effects_and_compositions/effect_groups/misc/vectorscope_MLT.rst:28
msgid "https://youtu.be/O1hbS6VZh_s"
msgstr "https://youtu.be/O1hbS6VZh_s"
