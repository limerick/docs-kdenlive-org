# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-11 15:12+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:14
msgid "Square Blur"
msgstr "Vierkante verdoezeling"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:18
msgid ""
"This is the `Frei0r squareblur <https://www.mltframework.org/plugins/"
"FilterFrei0r-squareblur/>`_ MLT filter."
msgstr ""
"Dit is het MLT-filter `Frei0r squareblur <https://www.mltframework.org/"
"plugins/FilterFrei0r-squareblur/>`_."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:20
msgid "Variable-size square blur."
msgstr "Variable grootte vierkante verdoezeling."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:22
msgid "https://youtu.be/mNP3LTy7nao"
msgstr "https://youtu.be/mNP3LTy7nao"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:24
msgid ""
"Not 100% sure if this sample is Square Blur or Box Blur. The caption on this "
"sample was translated from Spanish. Original Spanish was \"Difuminar de "
"cuadro\"*"
msgstr ""
"Niet 100% zeker of dit sample vierkante verdoezeling of Vak verdoezeling is. "
"Het opschrift op dit sample is vertaald uit het Spaans. Het originele Spaans "
"was \"Difuminar de cuadro\".*"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/square_blur.rst:26
msgid "https://youtu.be/dMJqPhO0DEQ"
msgstr "https://youtu.be/dMJqPhO0DEQ"
