# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-04 11:44+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/transitions/alphaover.rst:10
msgid "alphaover transition"
msgstr "overgang alphaover"

#: ../../effects_and_compositions/transitions/alphaover.rst:12
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/transitions/alphaover.rst:14
msgid ""
"This is the `Frei0r alphaover <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaover/>`_ MLT transition."
msgstr ""
"Dit is de MLT-transitie `Frei0r alphaover <https://www.mltframework.org/"
"plugins/TransitionFrei0r-alphaover/>`_."

#: ../../effects_and_compositions/transitions/alphaover.rst:16
msgid "The alpha OVER operation."
msgstr "De bewerking alpha-OVER."

#: ../../effects_and_compositions/transitions/alphaover.rst:18
msgid "Yellow clip has a triangle alpha shape with min = 0 and max =618."
msgstr "Gele clip heeft een driehoekige alfa-vorm met min = 0 en max =618."

#: ../../effects_and_compositions/transitions/alphaover.rst:20
msgid "Green clip has rectangle alpha shape with min=0 and max =1000."
msgstr "Groene clip heeft rechthoekige alfa-vorm met min=0 en max =1000."

#: ../../effects_and_compositions/transitions/alphaover.rst:22
msgid "alphaover is the transition in between."
msgstr "alphaover is de overgang er tussen."
