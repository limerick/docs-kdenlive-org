msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-15 00:22+0000\n"
"PO-Revision-Date: 2022-03-16 15:48\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_glossary___useful_information___disable_all_timeline_effects."
"pot\n"
"X-Crowdin-File-ID: 26303\n"

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:13
msgid "Disable All Timeline Effects"
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:15
msgid ""
"Did you know that you can **temporarily disable all timeline effects at once?"
"** This may be helpful when you want to do some timeline work, yet some "
"performance heavy effects slow down this work. Alternatively, you may want "
"to consider using Kdenlive’s timeline preview."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:24
msgid ""
"You’ll find the corresponding option in the main menu :menuselection:"
"`Timeline --> Disable Timeline Effects`. This disables or re-enables all "
"timeline effects, that is, timeline clip effects and track effects."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:26
msgid ""
"However, please note that prior to Kdenlive 16.08.1, track effects are not "
"properly disabled or re-enabled by :menuselection:`Timeline --> Disable "
"Timeline Effects`."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:28
msgid ""
"Please see :ref:`effects_everywhere` about how to temporarily disable bin "
"clip effects."
msgstr ""
