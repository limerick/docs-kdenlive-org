msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-03-16 15:48\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___artistic___charcoal."
"pot\n"
"X-Crowdin-File-ID: 26119\n"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:14
msgid "Charcoal"
msgstr "Charcoal - 炭笔画"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:16
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:18
msgid ""
"This is the `Charcoal <https://www.mltframework.org/plugins/FilterCharcoal/"
">`_ MLT filter."
msgstr ""
"这是 `Charcoal <https://www.mltframework.org/plugins/FilterCharcoal/>`_ MLT "
"滤镜。"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:20
msgid "Charcoal drawing effect."
msgstr "炭笔画绘画效果."

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:22
msgid "https://youtu.be/fI1YrZcT26k"
msgstr "https://youtu.be/fI1YrZcT26k"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:24
msgid "https://youtu.be/1XpJCI6tHSc"
msgstr "https://youtu.be/1XpJCI6tHSc"

#: ../../effects_and_compositions/effect_groups/artistic/charcoal.rst:26
msgid "https://youtu.be/m-_MFMSb75w"
msgstr "https://youtu.be/m-_MFMSb75w"
