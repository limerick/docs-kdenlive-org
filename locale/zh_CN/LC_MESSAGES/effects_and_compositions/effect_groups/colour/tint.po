msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-03-16 15:48\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour___tint."
"pot\n"
"X-Crowdin-File-ID: 26037\n"

#: ../../effects_and_compositions/effect_groups/colour/tint.rst:13
msgid "Tint"
msgstr "Tint - 染色"

#: ../../effects_and_compositions/effect_groups/colour/tint.rst:15
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/colour/tint.rst:17
msgid ""
"This is the `Frei0r tint0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-tint0r/>`_ MLT filter."
msgstr ""
"这是 `Frei0r tint0r <https://www.mltframework.org/plugins/FilterFrei0r-"
"tint0r/>`_ MLT 滤镜。"

#: ../../effects_and_compositions/effect_groups/colour/tint.rst:19
msgid "https://youtu.be/kq7w1ZdS6GI"
msgstr "https://youtu.be/kq7w1ZdS6GI"
