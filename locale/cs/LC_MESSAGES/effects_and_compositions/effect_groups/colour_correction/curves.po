# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vpelcak@suse.cz>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-18 09:41+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:14
msgid "Curves"
msgstr "Křivky"

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:18
msgid ""
"This is the `Frei0r curves <https://www.mltframework.org/plugins/"
"FilterFrei0r-curves/>`_ MLT filter."
msgstr ""
"Toto je `Frei0r curves <https://www.mltframework.org/plugins/FilterFrei0r-"
"curves/>`_ filtr MLT."

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:20
msgid "Adjusts luminance or color channel intensity with curve level mapping."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:22
msgid ""
"The :ref:`bezier_curves` filter is also an interface to this same Frei0r "
"filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:28
msgid "https://youtu.be/xJ0SyVzssfI"
msgstr "https://youtu.be/xJ0SyVzssfI"

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:30
msgid "https://youtu.be/Uiu6KfM3T9s"
msgstr "https://youtu.be/Uiu6KfM3T9s"

#: ../../effects_and_compositions/effect_groups/colour_correction/curves.rst:32
msgid "https://youtu.be/6S6SvdrxFBg"
msgstr "https://youtu.be/6S6SvdrxFBg"
