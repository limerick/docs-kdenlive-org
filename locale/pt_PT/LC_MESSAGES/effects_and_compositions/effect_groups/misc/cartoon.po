# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 16:17+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: MLT cartoon\n"

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:13
msgid "Cartoon"
msgstr "Banda Desenhada"

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:15
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:17
msgid ""
"This is the `Frei0r cartoon <https://www.mltframework.org/plugins/"
"FilterFrei0r-cartoon/>`_ MLT filter."
msgstr ""
"Este é o filtro do MLT `Frei0r cartoon <https://www.mltframework.org/plugins/"
"FilterFrei0r-cartoon/>`_."

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:19
msgid "Cartoonify video, do a form of edge detect."
msgstr ""
"Cria uma banda desenhada do vídeo, usando uma espécie de detecção de arestas."

#: ../../effects_and_compositions/effect_groups/misc/cartoon.rst:21
msgid "https://youtu.be/92fI4znypEo"
msgstr "https://youtu.be/92fI4znypEo"
