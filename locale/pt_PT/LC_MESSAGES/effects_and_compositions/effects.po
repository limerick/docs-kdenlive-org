# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-13 11:26+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../effects_and_compositions/effects.rst:20
msgid "Effects"
msgstr "Efeitos"

#: ../../effects_and_compositions/effects.rst:22
msgid "Contents"
msgstr "Conteúdo"

#: ../../effects_and_compositions/effects.rst:24
msgid ""
"Effects in **Kdenlive** can be used to modify the audio and video properties "
"of the source material."
msgstr ""

#: ../../effects_and_compositions/effects.rst:26
msgid ""
"You add effects to clips by choosing them from the **Effects Tab** and "
"dragging them onto a clip in the timeline. Or by selecting a clip in the "
"timeline and choosing the :menuselection:`Timeline --> Add Effect` menu item "
"or :menuselection:`Add Effect` from Clip in Timeline :ref:`right_click_menu` "
"menu . For more detail see :ref:`quickstart`."
msgstr ""

#: ../../effects_and_compositions/effects.rst:28
msgid ""
"The effects that are in play on a given clip can be viewed and edited via "
"the Properties Tab that displays when the clip in question is selected in "
"the timeline."
msgstr ""

#: ../../effects_and_compositions/effects.rst:30
msgid ""
"You can also apply effects to an entire track. This is achieved by dragging "
"an effect from the **Effect Tab** to the **Track Header**. Or you can click "
"on the track header and choose the :menuselection:`Timeline --> Add Effect` "
"menu item. Tracks which have effects added in this fashion will have a gold "
"star icon in the track header."
msgstr ""

#: ../../effects_and_compositions/effects.rst:36
msgid ""
"Figure 1a. Track header showing star icon indicating an effect has been "
"added to this track."
msgstr ""

#: ../../effects_and_compositions/effects.rst:39
msgid "Effects Tab"
msgstr ""

#: ../../effects_and_compositions/effects.rst:41
msgid ""
"Make the Effects Tab visible use the :menuselection:`View --> Effects` menu "
"item."
msgstr ""

#: ../../effects_and_compositions/effects.rst:47
msgid ""
"Figure 1b - effects tab. The Information icon (6) has been toggled so that "
"the tab displays a brief description of the selected effect at the bottom"
msgstr ""

#: ../../effects_and_compositions/effects.rst:49
msgid ""
"The effects tab has 5 different icons that cause the application to show and "
"hide different categories of effects."
msgstr ""

#: ../../effects_and_compositions/effects.rst:51
msgid ""
"By default the effects tab displays the \"Main Effects\" (Icon 1 selected). "
"These are the Audio Correction, Alpha/Transform, Analysis and data, Colour "
"and the Image adjustment categories."
msgstr ""

#: ../../effects_and_compositions/effects.rst:53
msgid ""
"Icon 2 makes it display all the video effects categories (This option hides "
"the Audio Correction category that is in the \"Main Effects\" and adds the "
"Misc and Motion categories)."
msgstr ""

#: ../../effects_and_compositions/effects.rst:55
msgid "Icon 3 makes it display all the Audio effects categories"
msgstr ""

#: ../../effects_and_compositions/effects.rst:57
msgid "Icon 4 is Custom effects"
msgstr ""

#: ../../effects_and_compositions/effects.rst:59
msgid ""
"Icon 5 is Favorite Effects. This is the same list that appears in the "
"\"Insert an Effect ...\" context sensitive menu obtained on a clip in the "
"timeline. An effect gets in this list by choosing *Add to Favorites* from "
"the context sensitive menu on each of the effects."
msgstr ""

#: ../../effects_and_compositions/effects.rst:61
msgid ""
"Icon 6 toggles the information display which - when on - shows a description "
"of what the effect does."
msgstr ""

#: ../../effects_and_compositions/effects.rst:63
msgid ""
"If you find that you are missing some effects or effects categories that you "
"know and love it is probably because you need to choose to display the "
"hidden effects by choosing an Icon other that Icon 1 - \"Main Effects\""
msgstr ""

#: ../../effects_and_compositions/effects.rst:65
msgid ""
"To add an effect to a clip, simply drag it from the **Effects Tab** to the "
"clip on the :ref:`timeline`."
msgstr ""

#: ../../effects_and_compositions/effects.rst:68
msgid "The Properties Tab and its Menu"
msgstr ""

#: ../../effects_and_compositions/effects.rst:71
msgid "The Properties Tab"
msgstr "A Página de Propriedades"

#: ../../effects_and_compositions/effects.rst:73
msgid ""
"The Properties Tab displays the settings for the effects on the currently "
"selected clip (Figure 3) or the settings for the currently selected "
"transition (Figure 4) depending on whether it is a clip or a transition that "
"is currently selected."
msgstr ""

#: ../../effects_and_compositions/effects.rst:78
msgid "Figure 3 - Properties when a clip with effects is selected"
msgstr ""

#: ../../effects_and_compositions/effects.rst:83
msgid "Figure 4 - Properties when a transition is selected"
msgstr ""

#: ../../effects_and_compositions/effects.rst:87
msgid ""
"Click the eye icon shown at 2 to temporarily disable the effect and toggle "
"the button to the state shown at 1. Click the empty eye icon shown at 1 to "
"re-enable a disabled effect and toggle the button to the state shown at 2."
msgstr ""

#: ../../effects_and_compositions/effects.rst:93
msgid "Toggle effect on and off"
msgstr ""

#: ../../effects_and_compositions/effects.rst:95
msgid ""
"To remove an effect from a clip, click the trash can icon labelled 3 in the "
"image below. The arrow labelled 1 minimizes the effect in the Properties "
"Window. And the icon labelled 2 brings up the Properties Tab menu shown."
msgstr ""

#: ../../effects_and_compositions/effects.rst:101
msgid "Reset Effect"
msgstr "Repor o Efeito"

#: ../../effects_and_compositions/effects.rst:103
msgid ""
"This reverts all the settings in the effect back to their default values."
msgstr ""

#: ../../effects_and_compositions/effects.rst:106
msgid "Save Effect"
msgstr "Gravar o Efeito"

#: ../../effects_and_compositions/effects.rst:108
msgid ""
"This allows you to save the current effect and all its settings. The saved "
"effect will appear in the Effect List in the Custom category."
msgstr ""

#: ../../effects_and_compositions/effects.rst:111
msgid "Create Group"
msgstr "Criar um Grupo"

#: ../../effects_and_compositions/effects.rst:113
msgid ""
"This creates an Effect Group. An Effect Group is a place holder for multiple "
"effects. You can then save the group of effects – they will the appear in "
"the Effect List, in the Custom Section. You can then later apply the whole "
"group of effects to other parts of the timeline."
msgstr ""

#: ../../effects_and_compositions/effects.rst:115
msgid ""
"To add effects to the group, first add the effect in question to a clip. "
"Then drag that effect and drop it onto the Effect Group in the Properties "
"Tab."
msgstr ""

#: ../../effects_and_compositions/effects.rst:118
msgid "Create Region"
msgstr ""

#: ../../effects_and_compositions/effects.rst:122
msgid ""
"The \"Create Region\" feature enables a user to apply an effect to a part of "
"a clip only. It is a really powerful feature but currently the UI is not "
"fully ready to get its full potential."
msgstr ""

#: ../../effects_and_compositions/effects.rst:124
msgid ""
"Basically, you add an effect to a clip – for example \"Sepia\" – then you go "
"in the effect's menu and select \"Create Region\". This will now open a file "
"dialog."
msgstr ""

#: ../../effects_and_compositions/effects.rst:126
msgid ""
"In that dialog, you need to point to an MLT clip with alpha transparency. "
"This is where Kdenlive is not 100% ready because there are many ways we "
"could create such clips, using for example a threshold filter or "
"rotoscoping. But as a start, let's say you can open any image with alpha "
"transparency, or a title clip created with Kdenlive. Then, the \"sepia\" "
"effect will only be applied on the non-transparent areas of this \"region\" "
"clip."
msgstr ""

#: ../../effects_and_compositions/effects.rst:129
msgid "Keyframes in effects"
msgstr ""

#: ../../effects_and_compositions/effects.rst:131
msgid ""
"Many effects use the concept of \"Keyframes\". Keyframes are user-defined "
"points in your clip where you want an effect to start, stop or change. You "
"can set the parameters for your effects to different values at different "
"keyframes and **Kdenlive** will then gradually change the parameters between "
"the two keyframes so that by the time the video has arrived at the next "
"keyframe it will have adjusted the parameter to match that key frame. It "
"interpolates between keyframes."
msgstr ""

#: ../../effects_and_compositions/effects.rst:133
msgid ""
"See :ref:`quickstart` for an example on keyframing the RGB adjustment effect."
msgstr ""

#: ../../effects_and_compositions/effects.rst:137
msgid "Effect panels get zoom bars. Adjusting keyframes just get easier."
msgstr ""

#: ../../effects_and_compositions/effects.rst:144
msgid "Working with keyframes in the effect stack"
msgstr ""

#: ../../effects_and_compositions/effects.rst:148
msgid ""
"The effect’s keyframe panel has new icons, improved keyframe grabbing and "
"new functions like:"
msgstr ""

#: ../../effects_and_compositions/effects.rst:155
msgid "Select the keyframe you want to move"
msgstr ""

#: ../../effects_and_compositions/effects.rst:156
msgid "Move the cursor to the position where you want to move the keyframe to"
msgstr ""

#: ../../effects_and_compositions/effects.rst:157
msgid "Click on :menuselection:`Move selected keyframe to cursor position`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:166
msgid "Select the keyframe you want to duplicate"
msgstr ""

#: ../../effects_and_compositions/effects.rst:167
msgid ""
"Move the cursor to the position where you want to insert the new keyframe"
msgstr ""

#: ../../effects_and_compositions/effects.rst:168
msgid "Click on :menuselection:`Duplicate selected keyframe`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:177
msgid "Select all keyframes you want to apply the value on"
msgstr ""

#: ../../effects_and_compositions/effects.rst:178
msgid "Go to one of the selected keyframes and change the value(s) as you want"
msgstr ""

#: ../../effects_and_compositions/effects.rst:179
msgid "Click on :menuselection:`Apply values to selected keyframes`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:180
msgid ""
"Select the parameters you want to apply and click on :menuselection:`OK`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:188
msgid "Select keyframes with :kbd:`Ctrl + click`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:194
msgid "Rubber select select keyframes with :kbd:`Shift + click`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:200
msgid "Move multiple keyframes at once"
msgstr ""

#: ../../effects_and_compositions/effects.rst:205
msgid "Exchange keyframes across effects"
msgstr ""

#: ../../effects_and_compositions/effects.rst:207
msgid ""
"You can import and export keyframes from/to the clipboard. This feature is "
"not only useful to copy keyframes from one clip to another, it can e.g. also "
"be used to copy the results of the motion tracker to a transform effect."
msgstr ""

#: ../../effects_and_compositions/effects.rst:209
msgid ""
"To export the keyframes the clipboard click on |application-menu| inside the "
"keyframe widget and choose :menuselection:`Copy keyframes to clipboard`."
msgstr ""

#: ../../effects_and_compositions/effects.rst:215
msgid ""
"To import keyframes from the clipboard click on |application-menu| inside "
"the keyframe widget and choose :menuselection:`Import keyframes from "
"clipboard`. If you have valid data on your clipboard you should see a dialog "
"similar to the screenshot where you can adjust the mapping of the data."
msgstr ""

#: ../../effects_and_compositions/effects.rst:220
#, fuzzy
#| msgid "Save Effect"
msgid "Master effect"
msgstr "Gravar o Efeito"

#: ../../effects_and_compositions/effects.rst:224
msgid ""
"The ability to apply audio or video effects to all tracks. See :ref:"
"`master_effect`"
msgstr ""

#: ../../effects_and_compositions/effects.rst:227
msgid "Effect Zones"
msgstr ""

#: ../../effects_and_compositions/effects.rst:231
msgid ""
"The new Effect Zones allow you to apply effects to specific regions of "
"tracks or the timeline. Zones can be set from the effect zone bar in the "
"timeline or from the interface in the effect panel."
msgstr ""

#: ../../effects_and_compositions/effects.rst:233
msgid "**Track Effect Zone**"
msgstr ""

#: ../../effects_and_compositions/effects.rst:238
msgid "**Master Effect Zone**"
msgstr ""

#: ../../effects_and_compositions/effects.rst:244
msgid "Time Remapping (Speed Ramps)"
msgstr ""

#: ../../effects_and_compositions/effects.rst:248
msgid "The new Time Remap feature allows to keyframe the speed of a clip."
msgstr ""

#: ../../effects_and_compositions/effects.rst:255
#: ../../effects_and_compositions/effects.rst:259
#, fuzzy
#| msgid "Save Effect"
msgid "Masking Effects"
msgstr "Gravar o Efeito"

#: ../../effects_and_compositions/effects.rst:261
msgid ""
"You may now apply effects to only affect specific regions of a clip by using "
"masks. Do note that this is only the initial implementation so expect an "
"improved workflow in the coming releases. For now the process involves 3 "
"steps:"
msgstr ""

#: ../../effects_and_compositions/effects.rst:263
msgid ""
"Add one of the 3 available masks: Shape alpha (mask), Rotoscoping (mask) or "
"Alpha shapes (mask)."
msgstr ""

#: ../../effects_and_compositions/effects.rst:264
msgid "Add an effect (or effects) to be applied to the masked region."
msgstr ""

#: ../../effects_and_compositions/effects.rst:265
msgid "Add Mask Apply to activate the mask to the effects in step 2."
msgstr ""

#: ../../effects_and_compositions/effects.rst:270
msgid ""
"You may apply more than one mask per clip by following the same 3 step "
"process."
msgstr ""

#: ../../effects_and_compositions/effects.rst:273
msgid "Seek To Active Frame"
msgstr ""

#: ../../effects_and_compositions/effects.rst:275
msgid "Some keyframe controls have a *seek to active frame* button"
msgstr ""

#: ../../effects_and_compositions/effects.rst:281
msgid ""
"(Labeled 1 in screenshot A below). When *seek to active frame* is toggled on "
"and you click on one of the keyframes in the keyframe list, Kdenlive will "
"scroll the preview window to that keyframe. In the example of the "
"screenshot, we have selected the keyframe at 9:20 in A and the clip position "
"caret (highlighted in red box) shows the location of this keyframe. Clicking "
"the keyframe at 10:00 in B shows how the clip position has moved."
msgstr ""

#: ../../effects_and_compositions/effects.rst:288
msgid "Effects Demos"
msgstr ""

#: ../../effects_and_compositions/effects.rst:290
msgid ""
"The following three YouTube videos display the results of a number of the "
"video effects available in **Kdenlive** (Spanish captioning)."
msgstr ""

#: ../../effects_and_compositions/effects.rst:292
msgid "https://youtu.be/C6oeu2Yc64I"
msgstr ""

#: ../../effects_and_compositions/effects.rst:294
msgid "https://youtu.be/jrC4F_G64jA"
msgstr ""

#: ../../effects_and_compositions/effects.rst:296
msgid "https://youtu.be/XMoSgHHbA4k"
msgstr ""

#: ../../effects_and_compositions/effects.rst:298
msgid "Another YouTube video (English Captions)."
msgstr ""

#: ../../effects_and_compositions/effects.rst:300
msgid "https://youtu.be/capV7lUzbOw"
msgstr ""

#: ../../effects_and_compositions/effects.rst:302
msgid ""
"See also this YouTube play list from `Franz M.P. <http://www.youtube.com/"
"playlist?list=PLc1VErdvjnSFE6w6sryFWIu4lfKavhnvz>`_"
msgstr ""

#: ../../effects_and_compositions/effects.rst:305
msgid "Effects Categories"
msgstr ""

#: ../../effects_and_compositions/effects.rst:307
msgid ""
"See also :ref:`effects_and_transitions` for an alphabetical list of effects "
"and transitions."
msgstr ""

#: ../../effects_and_compositions/effects.rst:309
msgid "The effects are divided into the following categories:"
msgstr ""

#: ../../effects_and_compositions/effects.rst:317
msgid ""
"The available effects are defined by :file:`.xml` files found in :file:`."
"local/share/kdenlive/effects` (e.g. :file:`/usr/share/kdenlive/effects`)."
msgstr ""

#: ../../effects_and_compositions/effects.rst:319
msgid ""
"These :file:`.xml` files contain the default values for the effects "
"parameters. So if you don't like the default values for the effects in "
"Kdenlive, you can modify the defaults by editing these :file:`.xml` files."
msgstr ""

#: ../../effects_and_compositions/effects.rst:322
msgid "FAQ"
msgstr "FAQ"

#: ../../effects_and_compositions/effects.rst:324
msgid "Q: How to duplicate an effect to use similar settings somewhere else?"
msgstr ""

#: ../../effects_and_compositions/effects.rst:326
msgid ""
"A: Select your effect in the timeline. In the Properties Tab choose :"
"menuselection:`Save` (from the :ref:`effects`). You will now find this "
"effect available in the **Effect List** :menuselection:`Custom` section."
msgstr ""

#: ../../effects_and_compositions/effects.rst:328
msgid ""
"A: Other solution: select a strip containing the effect, :menuselection:"
"`Copy`, then, where you want to apply it again, right-click and select :"
"menuselection:`Paste effect` instead of :menuselection:`Paste`."
msgstr ""

#: ../../effects_and_compositions/effects.rst:330
msgid "Q: How to apply an effect on several clips (all) at the same time?"
msgstr ""

#: ../../effects_and_compositions/effects.rst:332
msgid ""
"A: You can select multiple clips with :kbd:`Shift + drag` (left mouse "
"button) around them. Then right-click and group clips (or :kbd:`Ctrl + G`)."
msgstr ""

#~ msgid "Contents:"
#~ msgstr "Conteúdo:"
