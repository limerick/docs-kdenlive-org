# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-11-17 11:24+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:16
msgid "Position and Zoom"
msgstr "Poloha a priblíženie"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:18
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:20
msgid ""
"Adjust size and position of clip using smooth affine transformations. "
"Formerly known as \"Pan and Zoom\"."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:22
msgid ""
"In this example we have two keyframes in the pan and zoom, one at the "
"beginning and one at the end. Size is 25% at the start keyframe and 100% at "
"the end. The images are centered on the screen at both keyframes."
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:24
msgid "https://youtu.be/0aSe1y6e4RE"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:26
msgid "See also this :ref:`blue_screen` that describes how to use:"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:28
msgid "Alpha Manipulation -> :ref:`blue_screen`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:29
msgid ":ref:`rotoscoping`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:30
msgid ":ref:`composite`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:31
msgid "Crop and Transform -> Pan and Zoom effect"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:32
msgid "Enhancement -> :ref:`sharpen`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:33
msgid "Alpha Manipulation -> :ref:`alpha_operations`"
msgstr ""

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:35
msgid ""
"`Tutorial: How to do pan and zoom with Kdenlive video editor - Peter "
"Thomson(YouTube) <https://youtu.be/B8ZPoWaxQrA>`_"
msgstr ""
