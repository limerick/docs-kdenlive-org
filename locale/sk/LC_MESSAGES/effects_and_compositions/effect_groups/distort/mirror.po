# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-17 11:25+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:13
msgid "Mirror"
msgstr "Zrkadliť"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:15
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:17
msgid ""
"This is the `Mirror <https://www.mltframework.org/plugins/FilterMirror/>`_ "
"MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:19
msgid "Provides various mirror and image reversing effects."
msgstr ""

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:21
msgid "https://youtu.be/ao32j0dSVII"
msgstr "https://youtu.be/ao32j0dSVII"

#: ../../effects_and_compositions/effect_groups/distort/mirror.rst:23
msgid "https://youtu.be/3-hcMZu52Vk"
msgstr "https://youtu.be/3-hcMZu52Vk"
