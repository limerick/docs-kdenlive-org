# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-17 11:50+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:12
msgid "Clip Menu — Replace Clip"
msgstr ""

#: ../../user_interface/menu/clip_menu/replace_clip.rst:14
msgid "Contents"
msgstr "Obsah"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` a clip in the Project "
"Bin. Replace Clip will allow you to select a different file but keep all of "
"the uses on the timeline. This can be useful if you work first with "
"placeholder clips (i.e. low resolution) and on the end, before rendering, "
"you replace the clip with the final clip."
msgstr ""
