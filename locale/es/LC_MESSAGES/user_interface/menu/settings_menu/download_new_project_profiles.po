# Spanish translations for docs_kdenlive_org_user_interface___menu___settings_menu___download_new_project_profiles.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___settings_menu___download_new_project_profiles\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-29 00:22+0000\n"
"PO-Revision-Date: 2021-11-14 04:55+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/settings_menu/download_new_project_profiles.rst:11
msgid "Download New Project Profiles"
msgstr "Descargar nuevos perfiles de proyectos"

#: ../../user_interface/menu/settings_menu/download_new_project_profiles.rst:14
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/settings_menu/download_new_project_profiles.rst:16
msgid ""
"**Note:** Since around August, 2013 when the kdenlive.org web site was "
"refurbished, this feature has been unavailable and the \"Get Hot New Stuff\" "
"window will wait forever to update. See Mantis `3133 <https://bugs.kdenlive."
"org/view.php?id=3133>`_."
msgstr ""

#: ../../user_interface/menu/settings_menu/download_new_project_profiles.rst:18
msgid ""
"The Download new Render profiles is now working as at Oct 2015. However the "
"download New Project Profiles is still not working as at Oct 2015."
msgstr ""
