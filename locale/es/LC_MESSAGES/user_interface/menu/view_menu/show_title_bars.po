# Spanish translations for docs_kdenlive_org_user_interface___menu___view_menu___show_title_bars.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___view_menu___show_title_bars\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-14 05:04+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:15
msgid "Show Title Bars"
msgstr ""

#: ../../user_interface/menu/view_menu/show_title_bars.rst:17
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/view_menu/show_title_bars.rst:19
msgid ""
"This toggles the display of the title bar and control buttons on dockable "
"windows in **Kdenlive**."
msgstr ""
