# Spanish translations for docs_kdenlive_org_user_interface___menu___view_menu___spectogram.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_user_interface___menu___view_menu___spectogram\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2021-11-14 05:05+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/view_menu/spectogram.rst:14
msgid "Spectrogram"
msgstr "Espectrograma"

#: ../../user_interface/menu/view_menu/spectogram.rst:17
msgid "Contents"
msgstr "Contenido"

#: ../../user_interface/menu/view_menu/spectogram.rst:19
msgid ""
"This allows you to monitor the audio properties of your clip in detail. The "
"spectrogram displays the loudness (in decibels) of the audio at different "
"audio frequencies over the entire length of the clip.  In the spectrogram, "
"the horizontal axis represents the audio frequency and the loudness is "
"represented by the brightness (version <= 0.9.8) or the colour (version >= "
"0.9.10) of the pixel on the graph. The vertical axis represents frame number."
msgstr ""

#: ../../user_interface/menu/view_menu/spectogram.rst:34
msgid ""
"For more information see :ref:`Granjow's blog "
"<audio_spectrum_and_spectrogram>` on Spectrogram"
msgstr ""

#: ../../user_interface/menu/view_menu/spectogram.rst:38
msgid ":ref:`scopes_directx`"
msgstr ":ref:`scopes_directx`"
