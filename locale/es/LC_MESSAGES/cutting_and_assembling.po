# Spanish translations for docs_kdenlive_org_cutting_and_assembling.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: docs_kdenlive_org_cutting_and_assembling\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2021-11-15 18:36+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../cutting_and_assembling.rst:1
msgid "Start editing in the timeline with Kdenlive video editor"
msgstr ""

#: ../../cutting_and_assembling.rst:1
msgid ""
"KDE, Kdenlive, editing, timeline, documentation, user manual, video editor, "
"open source, free, learn, easy"
msgstr ""

#: ../../cutting_and_assembling.rst:15
msgid "Cutting and assembling"
msgstr "Corte y ensamblaje"

#: ../../cutting_and_assembling.rst:19
msgid "Contents:"
msgstr "Contenido:"
