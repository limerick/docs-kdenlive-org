# Spanish translations for docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___brightness.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___effect_groups___colour_correction___brightness\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-24 18:26+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:14
msgid "Brightness"
msgstr "Brillo"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:16
msgid "Contents"
msgstr "Contenido"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:18
msgid ""
"This is the `Frei0r brightness filter <https://www.mltframework.org/plugins/"
"FilterFrei0r-brightness/>`_ MLT filter."
msgstr ""
"Este es el filtro `Frei0r brightness filter <https://www.mltframework.org/"
"plugins/FilterFrei0r-brightness/>`_ de MLT."

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:20
msgid "Adjusts the brightness of a source image."
msgstr "Ajusta el brillo de una imagen fuente."

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:22
msgid "https://youtu.be/qDZVBPoaEzY"
msgstr "https://youtu.be/qDZVBPoaEzY"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:24
msgid "https://youtu.be/t4SfVod5zCE"
msgstr "https://youtu.be/t4SfVod5zCE"
