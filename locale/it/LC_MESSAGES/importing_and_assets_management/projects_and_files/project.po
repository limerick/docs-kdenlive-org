# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2022-02-27 15:07+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../importing_and_assets_management/projects_and_files/project.rst:16
msgid "Project File Details"
msgstr "Dettagli sui file di progetto"

#: ../../importing_and_assets_management/projects_and_files/project.rst:21
msgid "Contents"
msgstr "Contenuto"

#: ../../importing_and_assets_management/projects_and_files/project.rst:23
msgid ""
"**Kdenlive** projects consist in a singe :file:`.kdenlive` file (in XML "
"format), gathering :"
msgstr ""
"I progetti di **Kdenlive** sono costituiti da un singolo file :file:`."
"kdenlive` (in formato XML), che raccoglie:"

#: ../../importing_and_assets_management/projects_and_files/project.rst:26
msgid "target video and audio properties"
msgstr "il video di destinazione e le proprietà audio"

#: ../../importing_and_assets_management/projects_and_files/project.rst:28
msgid ""
"references to all the source materials (and to their lighter *proxies* work "
"copies)"
msgstr ""
"dei riferimenti a tutti i materiali di partenza (e alle loro copie di lavoro "
"più leggere, le *clip rappresentative*)"

#: ../../importing_and_assets_management/projects_and_files/project.rst:30
msgid ""
"clips arrangement on the timeline, with effects applied, and everything to "
"get the final result"
msgstr ""
"gli aggiustamenti delle clip nella linea temporale, con gli effetti "
"applicati e con tutto il necessario per ottenere il risultato finale"

#: ../../importing_and_assets_management/projects_and_files/project.rst:33
msgid ""
"Project files are associated with a working directory, in which **Kdenlive** "
"will generate *proxies* and *thumbs*, so that an overview of your media "
"always shows up quickly (if you move your project file, you should declare "
"the directory change in the project properties)."
msgstr ""
"I file di progetto sono associati ad una cartella di lavoro, all'interno "
"della quale **Kdenlive** genererà le *clip rappresentative* e le "
"*miniature*, in modo da poter mostrare velocemente una panoramica dei file "
"multimediali (se sposti il  file di progetto dovresti sempre dichiarare il "
"cambiamento della cartella nelle proprietà del progetto)."

#: ../../importing_and_assets_management/projects_and_files/project.rst:35
msgid ""
"A major refactoring of the project file fixes a long standing issue with the "
"decimal separator (comma/point) conflict causing many crashes."
msgstr ""
"Un'importante ristrutturazione dei file di progetto ha corretto un problema "
"di lunga data: il conflitto col separatore decimale (virgola o punto), che "
"provocava molti arresti anomali."

#: ../../importing_and_assets_management/projects_and_files/project.rst:40
msgid ""
"Projects created with 20.08 forward are not backwards compatible, that is, "
"you won’t be able to open your :file:`.kdenlive` project files with older "
"versions."
msgstr ""
"I progetti creati con le versioni 20.08 e successive non sono compatibili "
"con le versioni precedenti: non sarai quindi in grado di aprire i file di "
"progetto :file:`.kdenlive` con le versioni precedenti."
