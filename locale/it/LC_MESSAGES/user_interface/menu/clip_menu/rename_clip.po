# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-12-11 18:07+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:12
msgid "Rename Clip"
msgstr "Rinomina clip"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:14
msgid "Contents"
msgstr "Contenuto"

#: ../../user_interface/menu/clip_menu/rename_clip.rst:16
msgid ""
"Change the name of a clip in the Project Bin to an arbitrary name. Does not "
"rename the file on the file system."
msgstr ""
"Modifica il nome di una clip nel contenitore del progetto in un altro "
"arbitrario. Non rinomina il file nel file system."

#: ../../user_interface/menu/clip_menu/rename_clip.rst:18
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin."
msgstr ""
"Questa voce di menu è disponibile dall':ref:`project_tree` su una clip nel "
"contenitore del progetto."
