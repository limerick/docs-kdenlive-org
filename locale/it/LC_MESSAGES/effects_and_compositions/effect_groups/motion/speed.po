# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-07 00:38+0000\n"
"PO-Revision-Date: 2022-03-03 08:38+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:14
msgid "Motion Effects - Speed"
msgstr "Effetti di movimento - velocità"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:18
msgid "Use :ref:`change_speed` instead"
msgstr "Usa piuttosto :ref:`change_speed`"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:22
msgid "Contents"
msgstr "Contenuto"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:24
msgid ""
"Make clip play faster or slower. Use of this effect mutes the audio of the "
"clip."
msgstr ""
"Riproduce una clip più velocemente o più lentamente. L'uso di questo effetto "
"silenzia l'audio della clip."

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:30
msgid ""
"The *Stroboscope* setting defines the number frames the effect skips when "
"playing back. For example, if *Stroboscope* is set to 5 then the effect will "
"only show every fifth frame but will show these frames for five times as "
"long, producing a jumpy, stroboscopic effect."
msgstr ""
"L'impostazione *Stroboscopio* definisce il numero di fotogrammi che "
"l'effetto salta durante la riproduzione. Ad esempio, se  *Stroboscopio* è "
"impostato a 5, l'effetto mostrerà un fotogramma ogni 5, ma per un tempo 5 "
"volte superiore: in questo modo produrrà un effetto a salti, stroboscopico."

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:32
msgid ""
"It has been reported that the **Speed** effect does not work very well on "
"H.264-formatted source video. It is recommended to transcode your source "
"material into the DNxHD format and apply the **Speed** effect to that. "
"(`forum post <https://forum.kde.org/viewtopic.php?f=270&amp;t=121296&amp;"
"p=311629#p311427>`_)"
msgstr ""
"È stato segnalato che l'effetto **Velocità** non funziona molto bene sulle "
"sorgenti video in formato H.264. È quindi raccomandata prima la "
"transcodifica nel formato DNxHD, e poi l'applicazione dell'effetto "
"(`messaggio nel forum <https://forum.kde.org/viewtopic.php?f=270&amp;"
"t=121296&amp;p=311629#p311427>`_)."
