# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 00:38+0000\n"
"PO-Revision-Date: 2022-01-19 13:29+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:13
msgid "Adding meta data to mp4 video"
msgstr "Ajoute de métadonnées dans les vidéos « mp4 »"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:15
msgid ""
"Adding a useful information to your video files – such as title, authors, "
"the date of production, and some description – can be quite helpful to both "
"yourself and your customers. This additional data – or *meta data* – can be "
"used by media players to organize your many video files. Unfortunately, "
"while adding such meta data to audio files is straightforward, adding useful "
"meta data to video files is … a nightmare. But don’t despair, as we’ve "
"compiled some useful information for you!"
msgstr ""
"L'ajout d'informations utiles à vos fichiers vidéo - comme le titre, les "
"auteurs, la date de production et une description - peut être très utile "
"pour vous et vos clients. Ces données supplémentaires - ou *métadonnées* - "
"peuvent être utilisées par les lecteurs multimédia pour organiser vos "
"nombreux fichiers vidéo. Malheureusement, si l'ajout de ces métadonnées aux "
"fichiers audio est simple, l'ajout de métadonnées utiles aux fichiers vidéo "
"est... un cauchemar. Mais ne désespérez pas, car nous avons compilé quelques "
"informations utiles pour vous !"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:18
msgid "Project Meta Data"
msgstr "Métadonnées du projet"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:29
msgid ""
"Kdenlive allows you to specify the meta information that should be added to "
"a rendered file. Just go to :menuselection:`Project --> Project Settings`, "
"then select the second tab named :guilabel:`Metadata`."
msgstr ""
"Kdenlive vous permet de spécifier les métadonnées devant être ajoutées à un "
"fichier de rendu. Il suffit d'aller dans :menuselection:`Projet / Paramètres "
"du projet`, puis de sélectionner le deuxième onglet nommé :guilabel:"
"`Métadonnées`."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:31
msgid ""
"For your convenience, Kdenlive automatically adds the following four "
"elements to each new project:"
msgstr ""
"Pour votre confort, Kdenlive ajoute automatiquement les quatre éléments "
"suivants à chaque nouveau projet :"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:33
msgid "**title**."
msgstr "**titre**."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:34
msgid ""
"**author** – doesn’t work for .mp4 containers, but for .mov containers; you "
"may use **artist** instead with .mp4 containers."
msgstr ""
"**auteur** - ne fonctionne pas pour les conteneurs « .mp4 », mais pour les "
"conteneurs « .mov ». Vous pouvez utiliser **artiste** à la place avec les "
"conteneurs « .mp4 »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:35
msgid "**copyright**."
msgstr "**Droit d'auteur**."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:36
msgid ""
"**year** – please note that this element does not work for video rendered "
"into MP4 containers (such as .mov and .mp4). See below for more details. Use "
"a date element instead. Oh, this year element is actually quite pesky: you "
"can delete it, but it will automatically reappear. So simply ignore it … as "
"ffmpeg does too."
msgstr ""
"**année** - Veuillez noter que cet élément ne fonctionne pas pour les vidéos "
"rendues dans des conteneurs « MP4 » (tels que « .mov » et « .mp4 »). "
"Veuillez regarder ci-dessous pour plus de détails. Vous pouvez utiliser un "
"élément « date » à la place. Oh, cet élément « année » est en fait assez "
"embêtant : vous pouvez le supprimer mais il réapparaîtra automatiquement. Il "
"suffit donc de l'ignorer... exactement comme le fait aussi le logiciel "
"« ffmpeg »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:38
msgid ""
"But there are more elements that you actually may use with your MP4 "
"containers."
msgstr ""
"Mais, il existe d'autres éléments que vous pouvez utiliser avec vos "
"conteneurs « MP4 »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:42
msgid "MP4 Containers"
msgstr "Conteneurs « MP4 »"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:44
msgid ""
"Unfortunately, there are competing sets of meta data when working with MP4 "
"containers. The older, standard MP4 set is rather limited in what additional "
"meta data can be used. Then, there is Apple iTunes, and that brings in a lot "
"of meta data, and even changes the allowed set from time to time. We will "
"come back to this difference in a second."
msgstr ""
"Malheureusement, il existe des ensembles concurrents de métadonnées lorsque "
"l'on travaille avec des conteneurs « MP4 ». L'ancien ensemble « MP4 » "
"standard est plutôt limité en ce qui concerne les métadonnées "
"supplémentaires pouvant être utilisées. Ensuite, il y a Apple iTunes, "
"apportant beaucoup de métadonnées et modifiant même de temps en temps "
"l'ensemble autorisé. Cette différence sera détaillée dans une seconde."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:46
msgid ""
"Unfortunately, information about what meta data can be used in which "
"situation is rather scarce, sometimes outdated, and sometimes plainly wrong. "
"Of course, this may also apply to this Toolbox article, but I’ve taken much "
"effort to cross-check things in real life. As Kdenlive uses `ffmpeg <https://"
"www.ffmpeg.org/>`_ for encoding, one would think that there’s plenty of "
"information – unfortunately, it’s not, and the scarce information is "
"sometimes plainly wrong. At some point, I’ve resorted to reading the source "
"code in order to find out which so-called MP4 atoms actually are supported "
"and through which element names (the magic happens inside "
"`mov_write_ilst_tag()` in `moveenc.c <https://github.com/FFmpeg/FFmpeg/"
"blob/5a8b41b4a76fc6586ff6afff78e5f0aa7b25068a/libavformat/movenc."
"c#L2996>`__). Luckily, you don’t need to dive into the source anymore."
msgstr ""
"Malheureusement, les informations concernant quelles métadonnées utiliser "
"dans telle ou telle situation sont plutôt rares, parfois dépassées et "
"parfois tout simplement erronées. Bien sûr, cela peut aussi s'appliquer à "
"cet article sur la boîte à outils. Cependant, j'ai fait beaucoup d'efforts "
"pour vérifier les choses dans la vie réelle. Comme Kdenlive utilise le "
"logiciel « ffmpeg <https://www.ffmpeg.org/> »_ pour le codage, on pourrait "
"penser qu'il y a beaucoup d'informations. Malheureusement, ce n'est pas le "
"cas et les rares informations sont parfois carrément erronées. À un moment "
"donné, j'ai eu recours à la lecture du code source pour savoir quels "
"éléments « MP4 » sont réellement pris en charge et par quels noms d'éléments "
"(la magie se passe à l'intérieur de la fonction « mov_write_ilst_tag() » "
"dans le fichier source « moveenc.c <https://github.com/FFmpeg/FFmpeg/"
"blob/5a8b41b4a76fc6586ff6afff78e5f0aa7b25068a/libavformat/movenc."
"c#L2996> »_). Heureusement, vous n'avez pas besoin de vous plonger dans les "
"sources, du tout."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:48
msgid ""
"Finally, please note that other container formats, such as `Matroska "
"<https://en.wikipedia.org/wiki/Matroska>`_ (.mkv) or *cough* `Audio Video "
"Interleave <https://en.wikipedia.org/wiki/Audio_Video_Interleave>`_ (.avi), "
"support yet other sets of meta data elements. So, variance galore!"
msgstr ""
"Enfin, veuillez noter que d'autres formats de conteneurs, tels que "
"« Matroska <https://fr.wikipedia.org/wiki/Matroska> »_ (.mkv) ou *cough* "
"« Audio Video Interleave <https://fr.wikipedia.org/wiki/"
"Audio_Video_Interleave> »_ (.avi), prennent en charge d'autres ensembles "
"d'éléments de métadonnées. Donc, variance à gogo !"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:51
msgid "Ffmpeg Supported MP4 Container Meta Information"
msgstr ""
"Informations « ffmpeg » prises en chargement dans les métadonnées du "
"conteneur « MP4 »"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:53
msgid ""
"But now for the real meat: here come the available meta data elements that "
"ffmpeg supports."
msgstr ""
"Mais, il est temps de passer maintenant aux choses sérieuses : voici les "
"éléments de métadonnées disponibles, pris en charge par « ffmpeg »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:55
msgid ""
"Please note that you must use the ffmpeg meta data keys from the second "
"column in the table below as the metadata keys in Kdenlive’s Project "
"Metadata dialog!"
msgstr ""
"Veuillez noter que vous devez utiliser les clés de métadonnées « ffmpeg » de "
"la deuxième colonne du tableau ci-dessous comme clés de métadonnées dans la "
"boîte de dialogue « Métadonnées du projet » de Kdenlive !"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:56
msgid ""
"Ffmpeg supports different meta data elements, based on the type of "
"container. And this container type normally gets derived from the container "
"filename suffix (ending): in particular, .mp4 and .mov."
msgstr ""
"Ffmpeg prend en charge différents éléments de métadonnées, en fonction du "
"type de conteneur. Et ce type de conteneur est normalement dérivé du suffixe "
"(terminaison) du nom de fichier du conteneur : en particulier, « .mp4 » et "
"« .mov »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:61
msgid "Element"
msgstr "Élément"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:62
msgid "ffmpeg Meta Data Key"
msgstr "Clé de métadonnées « ffmpeg » "

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:63
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:115
msgid "Description"
msgstr "Description"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:64
msgid "MOV"
msgstr "MOV"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:65
msgid "MP4 (iTunes)"
msgstr "MP4 (iTunes)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:66
msgid "Tag"
msgstr "Étiquette"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:67
msgid "Title"
msgstr "Titre"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:68
msgid "title"
msgstr "titre"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:69
msgid "The title of this video. (String)"
msgstr "Le titre de la vidéo (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:70
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:71
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:76
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:77
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:82
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:83
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:88
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:89
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:95
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:100
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:112
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:119
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:124
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:130
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:131
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:137
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:142
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:143
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:148
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:154
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:160
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:167
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:173
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:179
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:185
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:191
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:197
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:203
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:209
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:215
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:221
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:227
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:232
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:239
msgid "✔"
msgstr "✔"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:72
msgid "©nam"
msgstr "©nam"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:73
msgid "Year"
msgstr "Année"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:74
msgid "date"
msgstr "date"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:75
msgid ""
"The date of production. Please note that the ffmpeg documentation is totally "
"wrong here, there is no key named year, but only date. (String)"
msgstr ""
"La date de production. Veuillez noter que la documentation de « ffmpeg » est "
"totalement erronée ici. Il n'y a aucune clé nommée « année » mais seulement "
"un clé « date » (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:78
msgid "©day"
msgstr "©jour"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:79
msgid "Copyright"
msgstr "Licence"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:80
msgid "copyright"
msgstr "Droit d'auteur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:81
msgid "The copyright of your video. (String)"
msgstr "La licence de la vidéo (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:84
msgid "©cpy"
msgstr "©cpy"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:85
msgid "Artist"
msgstr "Artiste"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:86
msgid "artist"
msgstr "artiste"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:87
msgid ""
"The name of the (video) artist. Please don’t use this element for the "
"composer, as there is a dedicated element especially for the composer, see "
"below. (String)"
msgstr ""
"Le nom de l'artiste (vidéo). Veuillez ne pas utiliser cet élément pour le "
"compositeur, car il existe un élément dédié spécialement pour le "
"compositeur. Veuillez consulter le sujet ci-dessous (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:90
msgid "©ART"
msgstr "©ART"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:91
msgid "Album Artist"
msgstr "Artiste de l'album"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:92
msgid "album_artist"
msgstr "artiste_album"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:93
msgid ""
"The name of the album artist: this may be a guest artist or a featured "
"artist. This element can also be left out or be the same name as the artist. "
"(String)"
msgstr ""
"Le nom de l'artiste de l'album : il peut s'agir d'un artiste invité ou d'un "
"artiste vedette. Cet élément peut également être omis ou porter le même nom "
"que l'artiste (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:96
msgid "aART"
msgstr "aART"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:97
msgid "Author"
msgstr "Auteur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:98
msgid "author"
msgstr "auteur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:99
msgid "The author of the video. (String)"
msgstr "L'auteur de la vidéo (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:102
msgid "©aut"
msgstr "©aut"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:103
msgid "Composer"
msgstr "compositeur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:104
msgid "composer"
msgstr "compositeur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:105
msgid "The name of the composer. (String)"
msgstr "Le nom du compositeur (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:108
msgid "©wrt"
msgstr "©wrt"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:109
msgid "Album"
msgstr "Album"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:110
msgid "album"
msgstr "album"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:111
msgid "The title or the name of this album. (String)"
msgstr "Le titre ou le nom de cet album (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:114
msgid "©alb"
msgstr "©alb"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:116
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:122
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:128
msgid "comment"
msgstr "commentaire"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:117
msgid ""
"A (content) description of this video. For a synopsis, please see the "
"separate element instead. (String)"
msgstr ""
"Une description (du contenu) de cette vidéo. Pour un résumé, veuillez plutôt "
"consulter l'élément séparé (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:120
msgid "desc"
msgstr "desc"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:121
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:127
msgid "Comment"
msgstr "Commentaire"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:123
msgid ""
"A (short) comment on your video. This will probably a comment set by the "
"audience, not at the time of production. (String)"
msgstr ""
"Un (court) commentaire sur votre vidéo. Il s'agira probablement d'un "
"commentaire défini par le public et non au moment de la production (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:126
msgid "©des"
msgstr "©des"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:129
msgid "Same as before, but encoded in a separate element. (String)"
msgstr "Identique au précédent mais encodé dans un élément séparé (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:132
msgid "©cmt"
msgstr "©cmt"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:133
msgid "Synopsis"
msgstr "Résumé"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:134
msgid "synopsis"
msgstr "résumé"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:135
msgid "A synopsis, a longer description of this video. (String)"
msgstr "Un résumé, une description plus longue de cette vidéo (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:138
msgid "ldes"
msgstr "Idées"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:139
msgid "Genre"
msgstr "Genre"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:140
msgid "genre"
msgstr "genre"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:141
msgid "The genre this video belongs to. (String)"
msgstr "Le genre à laquelle la vidéo appartient (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:144
msgid "©gen"
msgstr "©gen"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:145
msgid "Make"
msgstr "Réaliser"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:146
msgid "make"
msgstr "réaliser"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:147
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:153
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:159
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:207
msgid "(String)"
msgstr "(Chaîne)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:150
msgid "©mak"
msgstr "©mak"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:151
msgid "Model"
msgstr "Modèle"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:152
msgid "model"
msgstr "modèle"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:156
msgid "©mod"
msgstr "©mod"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:157
msgid "Location"
msgstr "Emplacement"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:158
msgid "location"
msgstr "emplacement"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:162
msgid "©xyz"
msgstr "©xyz"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:163
msgid "Grouping"
msgstr "Regroupement"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:164
msgid "grouping"
msgstr "regroupement"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:165
msgid ""
"The name of a group of videos somehow belonging together. In contrast to the "
"album element, grouping happens inside (that is, below) the album level. "
"(String)"
msgstr ""
"Le nom d'un groupe de vidéos appartenant à quelque chose de commun. "
"Contrairement à l'élément album, le regroupement se fait à l'intérieur "
"(c'est-à-dire en dessous) du niveau « Album » (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:168
msgid "©grp"
msgstr "©grp"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:169
msgid "Show"
msgstr "Afficher"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:170
msgid "show"
msgstr "afficher"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:171
msgid "The name of the TV show, if applicable. (String)"
msgstr "Le nom de l'émission de télévision, si applicable (Chaîne)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:174
msgid "tvsh"
msgstr "tvsh"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:175
msgid "Episode"
msgstr "Épisode"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:176
msgid "episode_id"
msgstr "id_épisode"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:177
msgid ""
"Either the episode name or episode number, for display. If necessary, use "
"the separate, yet optional episode number element for correct sorting. "
"(String)"
msgstr ""
"Soit le nom de l'épisode, soit le numéro de l'épisode, pour l'affichage. Si "
"nécessaire, veuillez utiliser l'élément séparé, mais facultatif, de numéro "
"d'épisode pour permettre un tri correct. (Chaîne)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:180
msgid "tven"
msgstr "tven"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:181
msgid "Episode (Sorting)"
msgstr "Épisode (Tri)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:182
msgid "episode_sort"
msgstr "épisode_tri"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:183
msgid ""
"This element is for sorting only, but never displayed. It allows numerical "
"sorting of episode names that are strings, but not (necessarily) numbers. "
"The valid range is limited to 0 to 255 only, so this doesn’t support all "
"those endless telenovas, it seems… (Int8)"
msgstr ""
"Cet élément ne sert que pour le tri, mais n'est jamais affiché. Il permet le "
"tri par clé numérique des noms d'épisodes qui sont des chaînes de "
"caractères, mais pas (nécessairement) des nombres. La plage de validité est "
"limitée entre 0 à 255 uniquement, ne permettant pas de prendre en charge "
"toutes les feuilletons interminables, semble-t-il... (Int8)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:186
msgid "tves"
msgstr "tves"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:187
msgid "Season"
msgstr "Saison"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:188
msgid "season_number"
msgstr "numéro_saison"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:189
msgid "The season number, in the range of 0 to 255 only. (Int8)"
msgstr "Le numéro de saison, dans un intervalle limité entre 0 et 255 (Int8)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:192
msgid "tvsn"
msgstr "tvsn"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:193
msgid "Lyrics"
msgstr "Paroles"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:194
msgid "lyrics"
msgstr "paroles"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:195
msgid "Optional lyrics for badly sung sing-along… (String)"
msgstr "Paroles optionnelles pour un chanson mal chantée... (String)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:198
msgid "©lyr"
msgstr "©lyr"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:199
msgid "Compilation"
msgstr "Compilation"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:200
msgid "compilation"
msgstr "compilation"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:201
msgid ""
"If 1, then this video file is part of a compilation. 0 otherwise. (Int8)"
msgstr ""
"Si l'option vaut 1, alors, le fichier de vidéo fait partie d'une "
"compilation, 0 sinon (Int8)."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:204
msgid "cpil"
msgstr "cpil"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:205
msgid "Network"
msgstr "Réseau"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:206
msgid "network"
msgstr "réseau"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:210
msgid "tvnn"
msgstr "tvnn"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:211
msgid "Media Type"
msgstr "Type de média"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:212
msgid "media_type"
msgstr "type_media"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:213
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:219
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:225
msgid "(Int8)"
msgstr "(Int8)"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:216
msgid "stik"
msgstr "stik"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:217
msgid "HD Video"
msgstr "Vidéo HD"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:218
msgid "hd_video"
msgstr "video_hd"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:222
msgid "hdvd"
msgstr "hdvd"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:223
msgid "Gapless Playback"
msgstr "Lecture sans interruption"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:224
msgid "gapless_playback"
msgstr "Lecture_sans_interruption"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:228
msgid "pgap"
msgstr "pgap"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:229
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:235
msgid "Encoding Tool"
msgstr "Outil d'encodage"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:230
msgid "encoder"
msgstr "encodeur"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:231
#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:237
msgid ""
"Not available to us users, as it gets automatically set by ffmpeg itself; "
"this is set to the libavformat version string."
msgstr ""
"Non disponible pour les utilisateurs, car automatiquement définie par le "
"logiciel « ffmpeg » lui-même. Ce paramètre est défini sous forme d'une "
"chaîne de version de « libavformat »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:234
msgid "©swr"
msgstr "©swr"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:236
msgid "encoding_tool"
msgstr "Outil_encodage"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:240
msgid "©too"
msgstr "©too"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:243
msgid "Notes"
msgstr "Notes"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:245
msgid ""
"There is no way to add cover art or DVD art to MP4 containers through "
"ffmpeg, and in consequence, in Kdenlive. Instead, you need to resort to "
"other video container tagging tools, such as `AtomicParsley <https://"
"sourceforge.net/p/atomicparsley/wiki/Home/>`_."
msgstr ""
"Il n'y a aucun moyen d'ajouter une pochette ou un DVD aux conteneurs « MP4 » "
"via le logiciel « ffmpeg », et par conséquent, dans Kdenlive. A la place, "
"vous devez recourir à d'autres outils de marquage de conteneurs vidéo, tels "
"que « AtomicParsley <https://sourceforge.net/p/atomicparsley/wiki/Home/> »_."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:247
msgid ""
"Kdenlive leverages ffmpeg for encoding, so if ffmpeg doesn’t support certain "
"atoms, there’s no way for Kdenlive to get it into the rendered output file."
msgstr ""
"Kdenlive utilise le logiciel « ffmpeg » pour l'encodage. Ainsi, si ce "
"logiciel ne prend pas en charge certaines spécificités, Kdenlive n'a aucun "
"moyen de les intégrer dans le fichier de rendu en sortie."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:250
msgid "Useful References"
msgstr "Références utiles"

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:252
msgid ""
"The following references give some more background information on ffmpeg and "
"meta data in .mov/.mp4 containers."
msgstr ""
"Les références suivantes donnent plus d'informations sur le logiciel "
"« ffmpeg » et les métadonnées dans les conteneurs « .mov / .mp4 »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:255
msgid ""
"First, and foremost, the ffmpeg source code for reference, and `moveenc.c "
"<https://sourceforge.net/p/atomicparsley/wiki/Home/>`_ in particular."
msgstr ""
"Tout d'abord, et avant tout, le code source de « ffmpeg » pour référence et "
"le fichier « moveenc.c <https://sourceforge.net/p/atomicparsley/wiki/Home/"
"> »_ en particulier."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:255
msgid ""
"`mov_write_ilst_tag()` is responsible to write the iTunes-compatible tags "
"for .mp4 containers."
msgstr ""
"La fonction « mov_write_ilst_tag() » prend en charge l'écriture des balises "
"compatibles avec iTunes pour les conteneurs « .mp4 »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:256
msgid ""
"`mov_write_udta_tag()` write the MPEG-standard tags instead, when using a ."
"mov container."
msgstr ""
"La fonction « mov_write_udta_tag() » écrit les balises standard « MPEG » à "
"la place, lors de l'utilisation d'un conteneur « .mov »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:257
msgid ""
"`How To: Create/Write ID3 tags using ffmpeg <https://jonhall.info/"
"create_id3_tags_using_ffmpeg/>`_ by Jon Hall. This article finally got me on "
"the right track. However, beware of a few incorrect ffmpeg keys in Jon’s "
"table (such as the TIT3 key, which I think isn’t correct); these may be due "
"to later changes in ffmpeg (or whatever). In the end, since some of Jon’s "
"keys didn’t work, I went for the ffmpeg source code which is the "
"authoritative source, of course. Nevertheless, I’m very thankful to Jon and "
"he is some of the rare really good sources with good insight into the topic. "
"Most other source just tell you how to press some buttons on some "
"application, but don’t give you any clue as to what is actually going on "
"behind the scenes."
msgstr ""
"« Tutoriel : comment Créer /Écrire des étiquettes « ID3 » en utilisant le "
"logiciel « ffmpeg » <https://jonhall.info/create_id3_tags_using_ffmpeg/> »_ "
"par Jon Hall. Cet article m'a finalement mis sur la bonne voie. Cependant, "
"attention à quelques clés « ffmpeg » incorrectes dans le tableau de Jon "
"(comme la clé « TIT3 », qui, je pense, n'est pas correcte). Cela peut être "
"dû à des changements ultérieurs de « ffmpeg » (ou autre chose). Finalement, "
"comme certaines des clés de Jon ne fonctionnaient pas, j'ai utilisé le code "
"source de « ffmpeg », qui fait autorité, bien sûr. Néanmoins, je suis très "
"reconnaissant à Jon. Il est l'une des rares sources vraiment fiables ayant "
"une bonne vision du sujet. La plupart des autres sources vous diront juste "
"comment appuyer sur certains boutons d'une application, mais ne vous "
"donneront aucun indice sur ce qui se passe réellement dans les coulisses."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:258
msgid ""
"`How To: Dump and Load metadata with ffmpeg <https://jonhall.info/how-to/"
"dump_and_load_metadata_with_ffmpeg/>`_ by Jon Hall. Sheds more light on how "
"to work with meta data when it comes to ffmpeg."
msgstr ""
"« Tutoriel : lister et charger des métadonnées avec le logiciel « ffmpeg » "
"<https://jonhall.info/how-to/dump_and_load_metadata_with_ffmpeg/> »_ par Jon "
"Hall. Fournit plus d'informations sur la façon de travailler avec les "
"métadonnées provenant du logiciel « ffmpeg »."

#: ../../glossary/useful_information/adding_meta_data_to_mp4_video.rst:259
msgid ""
"`FFmpeg Metadata <https://wiki.multimedia.cx/index.php?"
"title=FFmpeg_Metadata>`_ article from the `MultimediaWiki <https://wiki."
"multimedia.cx/index.php?title=Main_Page>`_."
msgstr ""
"Article « Métadonnées de « FFmpeg » <https://wiki.multimedia.cx/index.php?"
"title=FFmpeg_Metadata> »_ provenant de « MultimediaWiki <https://wiki."
"multimedia.cx/index.php?title=Main_Page> »_."
