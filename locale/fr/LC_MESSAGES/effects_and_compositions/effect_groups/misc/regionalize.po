# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-08 19:29+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:11
msgid "Regionalize"
msgstr "Appliquer à une région"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:13
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:15
msgid "Apply subeffects to a region defined by a clip's alpha channel."
msgstr ""
"Appliquer des sous-effets à une région définie par le canal alpha d'une "
"vidéo."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:17
msgid ""
"This is the `Region <https://www.mltframework.org/plugins/FilterRegion/>`_ "
"MLT filter."
msgstr ""
"Ceci est le filtre « Région <https://www.mltframework.org/plugins/"
"FilterRegion/> »_MLT."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:19
msgid "Arguments:"
msgstr "Arguments :"

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:21
msgid ""
"**File**  A file whose alpha channel will \"shape\" the region. The string "
"\"circle\" is a shortcut but it requires pixbuf with the librsvg loader. The "
"circle is automatically stretched to the region to create an ellipse."
msgstr ""
"**Fichier** Un fichier dont le canal alpha va « façonner » la région. La "
"chaîne « cercle » est un raccourci mais elle nécessite « pixbuf » avec le "
"chargeur « librsvg ». Le cercle est automatiquement étiré à la région pour "
"créer une ellipse."

#: ../../effects_and_compositions/effect_groups/misc/regionalize.rst:23
msgid ""
"**Region** Properties may be set on the encapsulated region transition. See "
"\"region\" transition for details."
msgstr ""
"Les propriétés **Région** peuvent être définies sur une transition de région "
"encapsulée. Veuillez consulter la transition « Région » pour plus de détails."
