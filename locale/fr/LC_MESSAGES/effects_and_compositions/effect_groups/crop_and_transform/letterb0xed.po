# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-26 13:50+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:13
msgid "LetterB0xed"
msgstr "LetterB0xed"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:17
msgid ""
"This is the `Frei0r letterb0xed <https://www.mltframework.org/plugins/"
"FilterFrei0r-letterb0xed/>`_ MLT filter."
msgstr ""
"Ceci est le filtre « Frei0r letterb0xed <https://www.mltframework.org/"
"plugins/FilterFrei0r-letterb0xed/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:19
msgid "Adds Black Borders at top and bottom for Cinema Look."
msgstr ""
"Ajoute des bordures noires sur les bords supérieurs et inférieurs pour une "
"apparence comme au cinéma."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:21
msgid "https://youtu.be/9Ldjt0QZPzs"
msgstr "https://youtu.be/9Ldjt0QZPzs"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/letterb0xed.rst:23
msgid "https://youtu.be/JBp8wQW-_Qw"
msgstr "https://youtu.be/JBp8wQW-_Qw"
