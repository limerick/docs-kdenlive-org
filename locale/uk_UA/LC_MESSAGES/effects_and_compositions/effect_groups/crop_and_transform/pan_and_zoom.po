# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-05 20:44+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:16
msgid "Position and Zoom"
msgstr "Позиціювання і зміна масштабу"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:18
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:20
msgid ""
"Adjust size and position of clip using smooth affine transformations. "
"Formerly known as \"Pan and Zoom\"."
msgstr ""
"Скоригувати розмір і розташування кліпу за допомогою плавних афінних "
"перетворень. Раніше ефект мав назву «Панорамування і масштабування»."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:22
msgid ""
"In this example we have two keyframes in the pan and zoom, one at the "
"beginning and one at the end. Size is 25% at the start keyframe and 100% at "
"the end. The images are centered on the screen at both keyframes."
msgstr ""
"У цьому прикладі у нас є два ключових кадрів у панорамуванні і масштабуванні "
"— один на початку і один наприкінці. Розміром є 25% у початковому ключовому "
"кадрі і 100% у кінцевому. Зображення центровано на екрані у обох ключових "
"кадрах."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:24
msgid "https://youtu.be/0aSe1y6e4RE"
msgstr "https://youtu.be/0aSe1y6e4RE"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:26
msgid "See also this :ref:`blue_screen` that describes how to use:"
msgstr ""
"Опис користування можна також знайти у :ref:`цьому розділі <blue_screen>`:"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:28
msgid "Alpha Manipulation -> :ref:`blue_screen`"
msgstr "Обробка прозорості -> :ref:`blue_screen`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:29
msgid ":ref:`rotoscoping`"
msgstr ":ref:`rotoscoping`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:30
msgid ":ref:`composite`"
msgstr ":ref:`composite`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:31
msgid "Crop and Transform -> Pan and Zoom effect"
msgstr "Обрізання і перетворення -> Ефект панорамування і масштабування"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:32
msgid "Enhancement -> :ref:`sharpen`"
msgstr "Покращення -> :ref:`sharpen`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:33
msgid "Alpha Manipulation -> :ref:`alpha_operations`"
msgstr "Керування прозорістю -> :ref:`alpha_operations`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:35
msgid ""
"`Tutorial: How to do pan and zoom with Kdenlive video editor - Peter "
"Thomson(YouTube) <https://youtu.be/B8ZPoWaxQrA>`_"
msgstr ""
"`Підручник: як виконувати панорамування та масштабування за допомогою "
"відеоредактора Kdenlive – Пітер Томсон (Peter Thomson) <https://"
"wwwyoutubecom/watch?v=BZPoWaxQrA>`_"
