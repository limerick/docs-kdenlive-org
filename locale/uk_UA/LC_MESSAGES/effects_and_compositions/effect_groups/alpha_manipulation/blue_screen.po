# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 19:08+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:14
msgid "Chroma Key"
msgstr "Хромакей"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:18
msgid ""
"This effect allows you to do Chroma Keying (also known as Green Screen or "
"Blue Screen) in **Kdenlive**. Chroma keying is where you remove backgrounds "
"of a similar colour. This effect is a very basic. For a more complication "
"alpha manipulation/background removal effect, use :ref:`color_selection`."
msgstr ""
"За допомогою цього ефекту можна скористатися хромакеєм (також відомим як "
"зелений екран або синій екран) у **Kdenlive**. За допомогою цієї технології "
"можна прибрати з зображення тло однакового кольору. Можливості ефекту є "
"доволі примітивними. Складніші варіанти керування прозорістю або вилучення "
"тла можна знайти у :ref:`ефекті вибору за кольором <color_selection>`."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:20
msgid "For black backgrounds, the :ref:`screen` is slightly more effective."
msgstr "Для чорного тла дещо ефективнішим є перехід :ref:`екран <screen>`."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:22
msgid "Here are some video tutorials on how to use chroma key."
msgstr "Нижче наведено декілька відеопідручників щодо використання хромакею."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:25
msgid "Basic Chroma Keying Tutorial"
msgstr "Базовий підручник з використання хромакею"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:27
msgid "Select the clip you want to chroma key in the timeline."
msgstr ""
"Позначте кліп, до якого ви хочете застосувати хромакей на монтажному столі."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:29
msgid ""
"Search for \"chroma key\" in the effects tab, and drag it onto the "
"properties tab."
msgstr ""
"Знайдіть пункт «Хромакей» на вкладці ефектів і перетягніть його на вкладку "
"властивостей."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:31
msgid ""
"Press on the button that looks like a water droplet, and then click on the "
"background of the video. This will chroma key out the background."
msgstr ""
"Клацніть на кнопці, яка виглядає як крапля води, а потім клацніть на тлі "
"відео. Хромакей буде застосовано до зовнішньої частини тла."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:33
msgid ""
"Use the variance slider to control the amount of background chroma keyed "
"out. This will require adjustment if your chroma key didn't immediately turn "
"out perfect."
msgstr ""
"Скористайтеся повзунком розсіювання для керування варіативністю вибору тла, "
"для якого буде використано ефекти хромакею. Цей параметр потребує "
"коригування, якщо використання однотонного тла не є ідеальним."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:36
msgid "Video Tutorial"
msgstr "Відеопідручник"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:38
msgid "https://youtu.be/bMwbffYIS40"
msgstr "https://youtu.be/bMwbffYIS40"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:41
msgid "See also"
msgstr "Див. також"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:43
msgid ""
":ref:`rotoscoping` effect. Rotoscoping is where you manual draw a region, "
"and everything outside/inside that region will disappear. This is useful for "
"backgrounds with multiple different colours."
msgstr ""
"Ефект :ref:`ротоскопа <rotoscoping>`. У ротоскопії ви малюєте ділянку, і "
"програма усуває усе зображення зовні або всередині цієї ділянки. Це корисно "
"для випадків із тлом, яке розфарбовано у декілька різних кольорів."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:45
msgid ""
":ref:`keysplillm0pup` effect in the Misc group. The Key Spill Mop Up effect "
"can be used to improve the edges of the blue screen effect - when edge "
"problems are caused by \"keyspill\". Keyspill is when the color of the "
"screen used for colorkeying spills onto the subject due to light reflection."
msgstr ""
"Ефект :ref:`поглинання відбиття ключового кольору <keysplillm0pup>` у групі "
"«інші». Ефектом поглинання відбиття ключового кольору (Key Spill Mop Up) "
"можна скористатися для удосконалення країв ділянок при застосуванні ефекту "
"синього екрана, якщо проблеми спричинено відбиттям кольору на об'єктах."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/blue_screen.rst:47
msgid ""
":ref:`color_selection` which also does color based alpha selection, but in a "
"much more detailed fashion. Use it for less contrasting or more complex "
"backgrounds."
msgstr ""
"Див. також :ref:`ефект вибору кольору <color_selection>`. Цим ефектом також "
"можна скористатися для перетворення кольору на прозорий, але із набагато "
"ширшим спектром параметрів. Цим ефектом можна скористатися для "
"неконтрастного або складного випадку тла."
