# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-12-04 21:41+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:13
msgid "Automatically Raising the Properties Pane"
msgstr "Автоматичне підняття панелі властивостей"

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:17
msgid ""
"Depending on your workflow habits, you can configure **when to raise the "
"properties pane** (in case it’s currently hidden)."
msgstr ""
"Залежно від ваших звичок у робочому процесі, ви можете налаштувати, **коли "
"програма підніматиме панель властивостей** (якщо її приховано)."

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:25
msgid ""
"To configure when Kdenlive should automatically raise the properties pane, "
"go to :menuselection:`Settings --> Configure Kdenlive` then tab :guilabel:"
"`Timeline`."
msgstr ""
"Щоб налаштувати, коли Kdenlive має автоматично піднімати панель "
"властивостей, скористайтеся пунктом меню :menuselection:`Параметри --> "
"Налаштувати Kdenlive`, потім на вкладку :guilabel:`Монтажний стіл`."

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:27
msgid ""
"You should now see a group titled **Raise properties pane when selecting in "
"timeline**, which contains these three check boxes:"
msgstr ""
"На цій вкладці ви побачите групу із заголовком **Піднімати панель "
"властивостей, якщо щось позначено на монтажному столі**, у якій містяться "
"такі три пункти:"

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:29
msgid "when selecting timeline **clips**,"
msgstr "якщо ви позначаєте **кліпи** на монтажному столі,"

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:30
msgid "when selecting **transitions**,"
msgstr "якщо ви позначаєте **переходи**,"

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:31
msgid "when selecting **tracks**."
msgstr "якщо ви позначаєте **доріжки**."

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:33
msgid ""
"Simply check the timeline elements for which you want the properties pane to "
"be automatically raised (shown) when you select them in the timeline."
msgstr ""
"Просто позначте елементи монтажного столу, для яких має бути автоматично "
"піднято (показано) панель властивостей, якщо ви позначаєте відповідні "
"елементи на монтажному столі."

#: ../../glossary/useful_information/automatically-raising-the-properties-pane.rst:35
msgid ""
"The default (classic) behavior of Kdenlive is to automatically raise the "
"properties pane for all clips, transitions, and tracks."
msgstr ""
"Типовою (класичною) поведінкою Kdenlive є автоматичне підняття панелі "
"властивостей для усіх кліпів, переходів і доріжок."
