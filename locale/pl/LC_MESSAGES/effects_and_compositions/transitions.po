# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-02-21 19:34+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.03.70\n"

#: ../../effects_and_compositions/transitions.rst:18
msgid "Transitions"
msgstr "Przejścia"

#: ../../effects_and_compositions/transitions.rst:20
msgid "Contents"
msgstr "Treść"

#: ../../effects_and_compositions/transitions.rst:22
msgid ""
"In **Kdenlive** a transition is a wipe or dissolve composition between two "
"overlapping clips."
msgstr ""
"W **Kdenlive** przejściem jest złożenia przetarcia lub rozpuszczenia "
"pomiędzy dwoma nachodzącymi na siebie wycinkami."

#: ../../effects_and_compositions/transitions.rst:24
msgid ""
"There are two ways of adding transitions in **Kdenlive**: **Mix clips**, aka "
"same tracks transitions, and the legacy way, which is transitions between "
"clips on different tracks."
msgstr ""
"Istnieją dwa sposoby dodawania przejść w **Kdenlive**: **Mieszanie "
"wycinków**, znane także jako przejścia tych samych ścieżek oraz stary "
"sposób, który polega na przejściu pomiędzy wycinkami na różnych ścieżkach."

#: ../../effects_and_compositions/transitions.rst:26
msgid ""
"Wipes are greyscale images in :file:`.pgm` (Portable GreyMap) format, and "
"during the transition the composition track will be displayed in the darkest "
"areas of the wipe image first. If the wipe is inversed, the composition "
"track will become visible in the brightest areas of the wipe image first "
"instead. You can download more wipes (by clicking the download button in the "
"wipe composition properties) or create your own and load them by clicking "
"the folder button."
msgstr ""
"Przetarcia są obrazami w odcieniach szarości w formacie :file:`.pgm` "
"(Portable GreyMap), a podczas przejścia, ścieżka złożenia najpierw zostanie "
"wyświetlona w najciemniejszych obszarach obrazu przetarcia. Jeśli przetarcie "
"jest odwrócone, to ścieżka złożenia będzie widoczna najpierw w "
"najjaśniejszych obszarach obrazu przetarcia. Możesz pobrać więcej przetarć "
"(naciskając przycisk pobierania we właściwościach złożenia przetarcia) lub "
"stworzyć swoje własne i wczytać je, naciskając na przycisk katalogu."

#: ../../effects_and_compositions/transitions.rst:31
msgid "Mixes"
msgstr "Mieszanie"

#: ../../effects_and_compositions/transitions.rst:35
msgid "Mixes are transitions between clips on the same track."
msgstr "Mieszaniny są przejściami pomiędzy wycinkami na tej samej ścieżce."

#: ../../effects_and_compositions/transitions.rst:41
msgid ""
"Kdenlive with mixed clips in the timeline. The toolbar button is circled "
"with red, and the transition properties are on the right."
msgstr ""
"Kdenlive ze zmieszanymi wycinkami na osi czasu. Przycisk paska narzędzi jest "
"wpisany w czerwony okrąg, a właściwości przejścia są po prawej."

#: ../../effects_and_compositions/transitions.rst:43
msgid "To use it:"
msgstr "Aby go użyć:"

#: ../../effects_and_compositions/transitions.rst:45
msgid ""
"Ensure that there is at least half a second worth of frames at the end of "
"both clips (outside the timeline clip). Without that, Kdenlive will not "
"apply the transition, but display an error message."
msgstr ""
"Upewnij się, że na końcach obu wycinków (na zewnątrz wycinka osi czasu) "
"istnieje co najmniej tyle klatek, aby wypełniły pół sekundy. Bez tego "
"Kdenlive nie nałoży przejścia i wyświetli błąd."

#: ../../effects_and_compositions/transitions.rst:46
msgid ""
"Select either clip. If there are clips in both ends of the one selected, the "
"transition will be added nearest the playhead."
msgstr ""
"Zaznacz dowolny wycinek. Jeśli na obu końcach zaznaczonego wycinka znajdują "
"się inne wycinki, to przejście zostanie dodane do miejsca najbliższego "
"głowicy odtwarzającej."

#: ../../effects_and_compositions/transitions.rst:48
msgid "Ready to add the transition:"
msgstr "Gotowy, aby dodać przejście:"

#: ../../effects_and_compositions/transitions.rst:50
msgid "**Keyboard:** Press the :kbd:`U` key."
msgstr "**Klawiatura:** Naciśnij przycisk :kbd:`U`."

#: ../../effects_and_compositions/transitions.rst:51
msgid ""
"**Mouse:** double-click at the point where the clips meet. This does not "
"require a clip to be selected."
msgstr ""
"**Mysz:** naciśnij dwukrotnie w miejscu, w którym wycinki się spotykają. Nie "
"wymaga to, aby wycinek był zaznaczony."

#: ../../effects_and_compositions/transitions.rst:52
msgid ""
"**Toolbar:** Press the :guilabel:`Mix Clips` button on the timeline toolbar."
msgstr ""
"**Pasek narzędzi:** Naciśnij przycisk :guilabel:`Zmieszaj wycinki` na pasku "
"narzędzi osi czasu."

#: ../../effects_and_compositions/transitions.rst:54
msgid ""
"The transition length defaults to one second equally distributed between the "
"two clips. You can drag either end of the transition to adjust."
msgstr ""
"Długość przejścia to domyślnie jedna sekunda równo rozłożona na dwa wycinki. "
"Możesz pociągnąć za dowolny koniec przejścia, aby go dostosować."

#: ../../effects_and_compositions/transitions.rst:56
msgid ""
"The default transition is dissolve, but you can select (click) the "
"transition and edit the wipe method and properties as desired in the effect/"
"transition stack window."
msgstr ""
"Domyślnym przejściem jest rozpuszczenie, lecz możesz zaznaczyć (nacisnąć) "
"przejście i edytować sposób przetarcia oraz właściwości tak jak wolisz w "
"oknie stosu efektów/przejść."

#: ../../effects_and_compositions/transitions.rst:58
msgid ""
"The composition method is set to Luma, and it does not make sense to change "
"that, since there will not really be a transition. Other methods are meant "
"for compositing two videos, not transitioning between them."
msgstr ""
"Sposób składania jest ustawiony na Lumę i nie ma sensu, aby to zmieniać, bo "
"nie będzie żadnego przejścia. Inne sposoby są przeznaczone do składania "
"dwóch nagrań, a nie do przechodzenia pomiędzy nimi."

#: ../../effects_and_compositions/transitions.rst:60
msgid "Select the desired wipe from Wipe Method."
msgstr "Wybierz żądane przetarcie ze sposobu przetarć."

#: ../../effects_and_compositions/transitions.rst:61
msgid "The :guilabel:`Reverse` option reverses the transition."
msgstr "Ustawienie :guilabel:`Wstecz` odgrywa przejście wstecz."

#: ../../effects_and_compositions/transitions.rst:62
msgid ""
"The :guilabel:`Softness:` slider will affect the edges of wipe transitions."
msgstr "Suwak :guilabel:`Miękkości:` wpłynie na krawędzie przejść przetarcia."

#: ../../effects_and_compositions/transitions.rst:63
msgid "Set the desired duration."
msgstr "Ustaw żądany czas trwania."

#: ../../effects_and_compositions/transitions.rst:64
msgid ""
"Since 21.08: select the alignment of the transition, either left, centered "
"(default) or right."
msgstr ""
"Od 21.08: wybierz wyrównanie przejścia, albo do lewej albo wyśrodkowane "
"(domyślnie) albo do prawej."

#: ../../effects_and_compositions/transitions.rst:66
msgid "To delete the transition, select it and press the :kbd:`Delete` key."
msgstr "Aby usunąć przejście, zaznacz je i naciśnij klawisz :kbd:`Delete`."

#: ../../effects_and_compositions/transitions.rst:70
msgid ""
"To apply only transitions on either the video or audio track just lock the "
"track on which you don’t want a transition to be applied and add transitions "
"by one of the above ways on the other track."
msgstr ""
"Aby nałożyć przejście tylko na ścieżce obrazie lub dźwięku, wystarczy że "
"zablokujesz ścieżkę, na której nie chcesz nałożyć przejścia i dodasz "
"przejścia na jeden z powyższych sposobów do innych ścieżek."

#: ../../effects_and_compositions/transitions.rst:73
msgid "Compositions"
msgstr "Złożenia"

#: ../../effects_and_compositions/transitions.rst:75
msgid ""
"Compositions are transitions between clips on different tracks. This is the "
"legacy way of doing transitions in **Kdenlive**."
msgstr ""
"Złożenia są przejściami pomiędzy wycinkami na różnych ścieżkach. Jest to "
"stary sposób wykonywania przejść w **Kdenlive**."

#: ../../effects_and_compositions/transitions.rst:81
msgid ""
"To the left the purple one-click circle to add a wipe composition. In the "
"middle the composition bar, and on the right the properties."
msgstr ""
"Po lewej, purpurowy okrąg do dodawania złożenia przejścia na jedno "
"naciśniecie. W środku pasek złożenia, a po prawej właściwości."

#: ../../effects_and_compositions/transitions.rst:83
msgid "To add a transition:"
msgstr "Aby dodać przejście:"

#: ../../effects_and_compositions/transitions.rst:85
msgid "Adjust your clips so that they overlap."
msgstr "Dostosuj swoje wycinki tak, aby się nachodziły."

#: ../../effects_and_compositions/transitions.rst:86
msgid ""
"Hold the mouse wheel over the top clip bottom corner, and click the purple "
"circle appearing (the :ref:`status_bar` will say \"Click to add a composition"
"\"), or alternatively right-click either clip and select :menuselection:"
"`Insert a composition... --> Wipe`."
msgstr ""
"Przytrzymaj kółko myszy na dolnym narożniku górnego wycinka i naciśnij na "
"purpurowy okrąg, który właśnie się pojawił (:ref:`status_bar` wyświetli "
"\"Naciśnij, aby dodać złożenie\") lub zamiennie naciśnij prawym na dowolny "
"wycinek i wybierz :menuselection:`Wstaw złożenie... --> Przetarcie`."

#: ../../effects_and_compositions/transitions.rst:88
msgid ""
"The default transition is a dissolve, to change that select (click) the "
"composition bar to show the effect/composition stack window."
msgstr ""
"Domyślnym przejściem jest rozpuszczenie, aby to zmienić zaznacz (naciśnij) "
"na pasku przejścia, aby pokazać okno stosu efektów/przejść."

#: ../../effects_and_compositions/transitions.rst:90
msgid ""
"The transition, when added this way, will cover the overlapping area between "
"the clips."
msgstr ""
"Przejście, gdy dodane w ten sposób, pokryje nachodzące się obszary między "
"dwoma wycinkami."

#: ../../effects_and_compositions/transitions.rst:92
msgid "You can:"
msgstr "Możesz:"

#: ../../effects_and_compositions/transitions.rst:94
msgid ""
"Select a composition track. The default is \"Automatic\" which is likely the "
"correct choice in this case."
msgstr ""
"Zaznacz ścieżkę złożenia. Domyślne to \"Samoczynnie\", co prawdopodobnie "
"jest poprawnym wyborem w tym przypadku."

#: ../../effects_and_compositions/transitions.rst:95
msgid "Select a wipe."
msgstr "Wybierz przetarcie."

#: ../../effects_and_compositions/transitions.rst:96
msgid "Enable :guilabel:`Inverse` to inverse the wipe."
msgstr "Włącz :guilabel:`Odwróć`, aby odwrócić przetarcie."

#: ../../effects_and_compositions/transitions.rst:97
msgid ""
"Enable :guilabel:`Revert` to revert the order of the videos in the "
"transition (which you probably do not want)."
msgstr ""
"Włącz :guilabel:`Wstecz`, aby kolejność nagrań była wsteczna w przejściu "
"(czego prawdopodobnie nie chcesz)."

#: ../../effects_and_compositions/transitions.rst:99
msgid ""
"If you move or adjust your clips after adding the transition, you need to "
"refit it manually if desired. You can drag the entire transition with the "
"select tool, and adjust its duration by dragging either end."
msgstr ""
"Jeśli przesuniesz lub dostosujesz swoje wycinki po dodaniu przejścia, to "
"będziesz musiał je ponownie dopasować. Możesz przesunąć całe przejście "
"narzędziem zaznaczania i dostosować czas trwania, ciągnąc za jego dowolny "
"koniec."

#: ../../effects_and_compositions/transitions.rst:102
msgid "Available Transitions"
msgstr "Dostępne przejścia"
