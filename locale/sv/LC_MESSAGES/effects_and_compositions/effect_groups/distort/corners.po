# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-13 19:34+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/effect_groups/distort/corners.rst:13
msgid "Distort - Corners"
msgstr "Förvräng - Hörn"

#: ../../effects_and_compositions/effect_groups/distort/corners.rst:15
msgid "Contents"
msgstr "Innehåll"

#~ msgid ""
#~ "See `ttill's blog <http://kdenlive.org/users/ttill/perspective-image-"
#~ "placement>`_"
#~ msgstr ""
#~ "Se `ttills blogg <http://kdenlive.org/users/ttill/perspective-image-"
#~ "placement>`_"
