# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2021-11-13 18:48+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../troubleshooting/faq.rst:17
msgid "Frequently Asked Questions"
msgstr ""

#: ../../troubleshooting/faq.rst:20
msgid "Contents"
msgstr "Innehåll"

#: ../../troubleshooting/faq.rst:23
msgid "What components does Kdenlive use?"
msgstr ""

#: ../../troubleshooting/faq.rst:25
msgid ""
"In effect, **Kdenlive** is a front end to `melt <http://www.mltframework.org/"
"bin/view/MLT/MltMelt>`_ - which uses the MLT video framework. The MLT video "
"framework relies on the FFMPEG project. **Kdenlive** writes :file:`sh.mlt` "
"XML files that code the edit points and transitions and it then calls :file:"
"`/usr/bin/kdenlive_render` and :file:`/usr/bin/melt` to render the video."
msgstr ""

#: ../../troubleshooting/faq.rst:29
msgid "How do I fix Audio Sync Issues?"
msgstr ""

#: ../../troubleshooting/faq.rst:31
msgid ""
"Often this is caused by having mixtures of different audio sources in the "
"project. Audio encoded with VBR (be it pure audio like MP3 or video with "
"audio) can be problematic."
msgstr ""

#: ../../troubleshooting/faq.rst:33
msgid ""
"If your audio source is from a video file try extracting the audio from your "
"video sources and transcoding the audio to WAV format. If you use the :ref:"
"`extract_audio` feature of Kdenlive to do this it will write a .wav file for "
"you."
msgstr ""

#: ../../troubleshooting/faq.rst:36
msgid "If you have a pure audio source try transcoding that to WAV."
msgstr ""
